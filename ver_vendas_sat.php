<?php 
	include 'conecta.php';

	$inicial    = $_GET['inicial'];
	$final      = $_GET['final'];

	if ($inicial == "" && $final == "") {
		$query = mysql_query("SELECT 
								    a.data_venda,
								    a.origem_venda,
								    a.num_nota_fiscal,
								    b.forma_pagamento,
								    a.valor_nota_fiscal
								    
								FROM
								    pde_fato_vendas a
								INNER JOIN
									forma_pagamento b
								ON
									a.id_forma_pagamento = b.id
								WHERE
								    sat_file != ''");

		
	} else {
		$query = mysql_query("SELECT 
								    a.data_venda,
								    a.origem_venda,
								    a.num_nota_fiscal,
								    b.forma_pagamento,
								    a.valor_nota_fiscal
								    
								FROM
								    pde_fato_vendas a
								INNER JOIN
									forma_pagamento b
								ON
									a.id_forma_pagamento = b.id
								WHERE
								    sat_file != ''
							    AND
							    	a.data_venda between STR_TO_DATE('".$inicial."', '%d/%m/%Y') and STR_TO_DATE('".$final."', '%d/%m/%Y')+1");
	}

	$result = '<br>
            <h4 class="ui center aligned header">Vendas Declaradas</h4>
            <table class="ui center aligned green celled selectable compact small table">
                <thead>
                  <tr>
                    <th>Data</th>
                    <th>N° Cupom</th>
                    <th>Canal de venda</th>
                    <th>Total do Cupom</th>
                    <th>Forma de Pagamento</th>
                  </tr>
                </thead>
                <tbody>
                ';

    while ($vendas_sat = mysql_fetch_array($query)) {
    	$result .= '<tr>';
    	$result	.= '<td>'.$vendas_sat['data_venda'].'</td>';
    	$result	.= '<td>'.$vendas_sat['num_nota_fiscal'].'</td>';
    	$result	.= '<td>'.$vendas_sat['origem_venda'].'</td>';
    	$result	.= '<td>R$ '.number_format($vendas_sat['valor_nota_fiscal'],2,",",".").'</td>';
    	$result	.= '<td>'.$vendas_sat['forma_pagamento'].'</td>';
    	$result .= '</tr>';
    	$subtotal = $subtotal + $vendas_sat['valor_nota_fiscal'];
    }

    $result .= '</tbody>';
    $result .= '<tfoot class="full-width">';
    $result .= '<tr>';
    $result .= '<th></th>';
    $result .= '<th></th>';
    $result .= '<th></th>';
    $result .= '<th><b>subtotal R$ '.number_format($subtotal,2,",",".").'</b></th>';
    $result .= '<th></th>';
    $result .= '</tr>';
    $result .= '</tfoot>';
    $result .= '</table>';

    echo $result;
 ?>