<?php 
	include 'cabecalho.php';
	include 'model.php';
	echo '<div class="ui active dimmer">
		    <div class="ui medium text loader">Gerando Cupom Fiscal...</div>
		  </div>';
	
	$br		= PHP_EOL; // Alterar variável de pular linha, caso dê problema no server
	//$br 	= "<br>";
	$nf 	= $_GET['nf'];
	$arquivo_sai = "../../../ACBrMonitorPLUS/sai.txt";
 	unlink($arquivo_sai);

	//Criamos uma função que recebe um texto como parâmetro.
	function gravar_ent($texto){
	    //Variável arquivo armazena o nome e extensão do arquivo.
	     $arquivo = "../../../ACBrMonitorPLUS/ent.txt";
	     
	     
	    //$arquivo = "sat/ent.txt";
	     
	    //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
	    if (file_exists($arquivo)) //verifica se o arquivo existe
	    {
	    	unlink($arquivo); // Caso exista, remove o arquivo antes de criar novamente
	    	$fp = fopen($arquivo, "a+"); // Recria o arquivo do zero
	    } else {
	    	$fp = fopen($arquivo, "a+"); // Se não existir, somente cria
	    }

	    // if (file_exists($arquivo_sai)) //verifica se o arquivo existe
	    // {
	    // 	unlink($arquivo_sai); // Caso exista, remove o arquivo antes de criar novamente	    	
	    // } 
	 
	    //Escreve no arquivo aberto.
	    fwrite($fp, $texto);
	     
	    //Fecha o arquivo.
	    fclose($fp);
	}

	// CONVERTER FORMA DE PAGAMENTO
	$forma = pde_fato_vendas(id_forma_pagamento,$nf);
	if ($forma == 1) {
		$cMP = 01;
		$cMP = str_pad($cMP, 2, '0', STR_PAD_LEFT);
		$aux = pde_movimentacao("valor",$nf);
		$vMP = $aux[1];
	} elseif ($forma == 2) {
		$cMP = 04;
		$cMP = str_pad($cMP, 2, '0', STR_PAD_LEFT);
		$vMP = pde_fato_vendas(valor_nota_fiscal,$nf);
	} elseif ($forma == 3) {
		$cMP = 03;
		$cMP = str_pad($cMP, 2, '0', STR_PAD_LEFT);
		$vMP = pde_fato_vendas(valor_nota_fiscal,$nf);
	}

	//$texto	=	'SAT.Inicializar()';
	$texto	.=	'SAT.CriarEnviarCFe('.$br;
	$texto	.= 	'[infCFe]'.$br;
	$texto	.= 	'versao='.s_config(versao_layout).$br;
	$texto	.=	'[Identificacao]'.$br;
	$texto	.=	'CNPJ='.s_config(cnpj_software).$br;
	$texto	.=	'signAC='.s_config(signac).$br;
	$texto	.=	'numeroCaixa=001'.$br;
	$texto	.=	'[Emitente]'.$br;
	$texto	.=	'CNPJ='.s_config(cnpj).$br;
	$texto	.=	'IE='.s_config(ie).$br;
	$texto	.=	'IM='.$br;
	$texto	.=	'cRegTrib='.s_config(cregtrib).$br;
	$texto	.=	'cRegTribISSQN='.$br;
	$texto	.=	'indRatISSQN='.$br;
	$texto	.=	'[Destinatario]'.$br;
	$texto	.=	'CNPJCPF='.pde_fato_vendas(cpf,$nf).$br;  // BUSCAR CPF DO CLIENTE
	$texto	.=	'xNome='.$br; // APENAS SE TIVER CADASTRO DO CLIENTE, PORÉM NÃO NECESSÁRIO
	$texto	.=	'[Entrega]'.$br;
	$texto	.=	'xLgr='.$br.'nro='.$br.'xCpl='.$br.'xBairro='.$br.'xMun='.$br.'UF='.$br;
	$rows = quantidadeItens($nf);
	$in = 1;
	for ($i=0; $i < $rows; $i++) { 
		$texto	.=	'[Produto00'.$in.']'.$br;
		$cProd	=	pde_fato_vendas_produtos(code,$nf);
		$texto	.=	'cProd='.$cProd[$in].$br;  // BUSCAR CÓDIGO DO PRODUTO (OBRIGATÓRIO)
		$texto	.=	'infAdProd='.$br; // CASO TENHA ALGUMA INFO ADICIONAL
		$texto	.=	'cEAN='.$br;  // DÚVIDA: DEVERÁ PREENCHER SOMENTE SE TIVER CÓD BARRAS? É OPCIONAL?
		$xProd	=	pde_fato_vendas_produtos(name,$nf);
		$texto	.=	'xProd='.$xProd[$in].$br; // BUSCAR NOME DO PRODUTO (OBRIGATÓRIO)
		$NCM 	=	pde_fato_vendas_produtos(ncm,$nf);
		$texto	.=	'NCM='.$NCM[$in].$br; // BUSCAR NO CADASTRO DO PRODUTO, CASO BRANCO 19022000
		$CFOP 	=	pde_fato_vendas_produtos(cfop,$nf);
		$texto	.=	'CFOP='.$CFOP[$in].$br; // BUSCAR NO CADASTRO DO PRODUTO, CASO BRANCO 5405
		$texto	.=	'uCom=UN'.$br; // BUSCAR NO CADASTRO DO PRODUTO (UN,KG,etc)
		$texto	.=	'Combustivel=0'.$br; // SEMPRE ZERO
		$qCom 	= pde_fato_vendas_produtos(quantidade,$nf);
		$texto	.=	'qCom='.number_format($qCom[$in],3,'.','').$br; // 2.0000 QUANTIDADE DA VENDA, USAR NUMBER_FORMAT 3
		$vUnCom = pde_fato_vendas_produtos(cost,$nf);
		$texto	.=	'vUnCom='.$vUnCom[$in].$br; // VALOR COM 2 CASAS DECIMAIS 35.00
		$texto	.=	'indRegra=A'.$br; // SEMPRE A
		$texto	.=	'vDesc=0'.$br; // SE TIVER DESCONTO NO ITEM
		$texto	.=	'vItem12741=0.09'.$br; // VALOR DO PRODUTO * VALOR DO IMPOSTO (P_12741)
		$texto	.=	'[ICMS00'.$in.']'.$br;
		$texto	.=	'Orig=0'.$br; // CADASTRO DO ITEM, SE ESTIVER BRANCO PREENCHER ZERO
		$texto	.=	'CSOSN=500'.$br; // Cadastro do produto icms_cso, se estiver em branco 500
		$texto	.=	'[PIS00'.$in.']'.$br;
		$texto	.=	'CST=49'.$br; // Cadastro do produto pis_cso, se estiver em branco 49 (obrigatório)
		$texto	.=	'[COFINS00'.$in.']'.$br;
		$texto	.=	'CST=49'.$br; // Cadastro do produto cofins_cso, se estiver em branco 49 (obrigatório)
		$in++;
	}

	$texto	.=	'[TOTAL]'.$br;
	$texto	.=	'vCFeLei12741=0.90'.$br; // TOTAL DA VENDA * VALOR DO IMPOSTO (P_12741)
	$texto	.=	'vAcresSubtot='.$br; // Se tiver acréscimo de valor na venda

	if ($forma == 4 || $forma == 5) {
		$idForma = 	pde_movimentacao("id_forma_pagamento",$nf);
		$vMP 	 =	pde_movimentacao("valor",$nf);
		for ($i=1; $i <= 2; $i++) { 
			if ($idForma[$i] == 2) {
			$cMP = 04;
			$cMP = str_pad($cMP, 2, '0', STR_PAD_LEFT);
			} elseif ($idForma[$i] == 3) {
			$cMP = 03;
			$cMP = str_pad($cMP, 2, '0', STR_PAD_LEFT);
			} elseif ($idForma[$i] == 1) {
			$cMP = 01;
			$cMP = str_pad($cMP, 2, '0', STR_PAD_LEFT);
			}
			$texto	.=	'[Pagto00'.$i.']'.$br;
			$texto	.=	'cMP='.$cMP.$br;
			$texto	.=	'vMP='.$vMP[$i].$br;
		}
		
	}
	else {

		$texto	.=	'[Pagto001]'.$br;
		$texto	.=	'cMP='.$cMP.$br; // Aqui a forma de pagamento conforme tabela abaixo:
		// 01 Dinheiro
		// 02 Cheque
		// 03 Cartão de Crédito
		// 04 Cartão de Débito
		// 05 Crédito Loja
		// 10 Vale Alimentação
		// 11 Vale Refeição
		// 12 Vale Presente
		// 13 Vale Combustível
		// 99 Outros
		$texto	.=	'vMP='.$vMP.$br; // Valor do pagamento
		// $texto	.=	'[Pagto001]'.$br;
		// $texto	.=	'cMP=01'.$br; // Código do pagamento
		// $texto	.=	'vMP=10'.$br; // Valor do pagamento
		// CASO TENHA PAGAMENTO QUEBRADO (METADE CARTÃO, METADE DINHEIRO, ETC...)
		// $texto	.=	'[Pagto002]'.$br;
		// $texto	.=	'cMP=04'.$br; 
		// $texto	.=	'vMP=50'.$br; 

	}

	$texto	.=	'[DadosAdicionais]'.$br;
	$texto	.=	'infCpl='.$br; // Se tiver informação adicional na venda em geral
	$texto	.=	')'.$br;
	//$texto	.=	'SAT.ImprimirExtratoVenda("C:\SAT\Vendas\AD35170811111111111111591234567890000014722801.xml","Caixa")';

	
	// GRAVAÇÃO DOS DADOS NO ARQUIVO TXT
	gravar_ent($texto); 
	//echo $texto;
	echo '<meta http-equiv="refresh" content="1; url=imprimirVendaSat.php?nf='.$nf.'">';
 ?>
