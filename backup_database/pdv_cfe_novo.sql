-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: pdv_cfe
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bkp_produtos`
--

DROP TABLE IF EXISTS `bkp_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bkp_produtos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` char(255) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `price` decimal(25,2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT 'no_image.png',
  `tax` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT '1',
  `quantity` decimal(15,2) DEFAULT '0.00',
  `barcode_symbology` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'code39',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'standard',
  `details` text CHARACTER SET utf8,
  `alert_quantity` decimal(10,2) DEFAULT '0.00',
  `cozinha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bkp_produtos`
--

LOCK TABLES `bkp_produtos` WRITE;
/*!40000 ALTER TABLE `bkp_produtos` DISABLE KEYS */;
INSERT INTO `bkp_produtos` VALUES (1,'0001','Hamburquer',2,2.00,'99ba81363ddbfe5a92c93023e1fd550a.jpg','0',4.00,0,53.00,'code39','standard','Hamburguer com P?o de Hamburguer, queijo, carne, presunto e salada',5.00,1),(2,'0002','Mixto Quente',2,1.00,'3ba18844e23b27e8224f8fa6b1752208.jpg','0',3.00,0,8.00,'code39','standard','',5.00,1),(3,'0003','Cahorro Quente',2,2.00,'573bc5101fabefd864960416b1752899.jpg','0',3.00,0,4.00,'code39','standard','',5.00,1),(4,'0004','Bolo de Chocolate',2,2.00,'8ad58758122f3a886e859def53da6a6a.jpg','0',3.00,0,7.00,'code39','standard','',5.00,0),(5,'0005','Coxinha de Frango',2,2.00,'d3115abf501ce492bdf449f72f185fb1.jpg','0',3.00,0,9.00,'code39','standard','',5.00,0),(6,'0006','Empada',2,2.00,'76fed631b7861010869172aa83d78e0a.jpg','0',3.00,0,19.00,'code39','standard','',5.00,0),(7,'0007','Monteiro Lopes',2,2.00,'3274477f5b7d3ef257c4562c56ef387e.jpg','0',3.00,0,10.00,'code39','standard','',5.00,0),(8,'0008','Risole de Carne',2,2.00,'32a3ac97716a9dc68812aecbaf11840a.jpg','0',4.00,0,4.00,'code39','standard','',5.00,0),(9,'0009','Coxinha de Caranguejo',2,4.00,'8bd5b89b645b1bc2d4d08816b5ad3d0b.jpg','0',6.00,0,6.00,'code39','standard','',5.00,0),(10,'0010','Coxinha de Camar?o',2,4.00,'272825062f261b126f1996ed099b4b87.jpg','0',6.00,0,7.00,'code39','standard','',5.00,0),(11,'0011','Sonho',2,2.00,'1f56837339171226e7e33eb0c5e8eae0.jpg','0',3.00,0,7.00,'code39','standard','',5.00,0),(12,'0012','Lasanha',2,6.00,'fd1c25461a5fbb0597c68bb78100c6ec.jpg','0',9.00,0,10.00,'code39','standard','',5.00,0),(13,'0013','Torta de Chocolate',2,3.00,'11fcdf61a2d8c2d6b7c3e9c0a6996a54.jpg','0',6.00,0,10.00,'code39','standard','',5.00,0),(14,'0014','Fanta Laranja Lata',1,2.00,'f0ed23add960528f5da95d8fb2a8a106.jpg','0',4.00,0,10.00,'code39','standard','',5.00,0),(15,'0015','Coca-Cola Lata',1,2.00,'d1ae8344e2fdfc3fcd80a96bb1f00240.jpg','0',4.00,0,7.00,'code39','standard','',5.00,0),(16,'0016','?gua Mineral',1,2.00,'91b3bcff369f45e167c3544bad752912.jpg','0',3.00,0,9.00,'code39','standard','',5.00,0),(17,'0017','Suco de Laranja',1,4.00,'f4cab501731cb47389a6c1a9a54cf736.jpg','0',6.00,0,5.00,'code39','standard','',5.00,0),(18,'01','Combo M',2,10.99,'no_image.png','5',8.71,0,0.00,'code39','combo','',0.00,0),(19,'02','Batata M',2,7.99,'no_image.png','0',4.72,0,0.00,'code39','standard','',0.00,0),(20,'03','Cobertura Cheddar',2,1.00,'no_image.png','0',0.27,0,0.00,'code39','standard','',0.00,0),(21,'012','Pizza Calabresa',3,24.00,'no_image.png','0',12.00,1,10.00,'code39','standard','',1.00,1),(22,'20','Pizza Mussarela',3,26.00,'no_image.png','0',13.00,1,10.00,'code39','standard','',0.00,1),(23,'027','1/2 Pizza Mussarela',3,26.00,'no_image.png','0',6.00,0,11.00,'code39','standard','',0.00,1),(24,'01777','1/2 Calabresa',3,21.00,'no_image.png','0',6.00,0,9.00,'code39','standard','',0.00,1);
/*!40000 ALTER TABLE `bkp_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caixa01`
--

DROP TABLE IF EXISTS `caixa01`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caixa01` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caixa01`
--

LOCK TABLES `caixa01` WRITE;
/*!40000 ALTER TABLE `caixa01` DISABLE KEYS */;
INSERT INTO `caixa01` VALUES (1,'0000-00-00 00:00','0000-00-00 00:00',0.00,0.00,'Fechado',1),(19,'2017-02-15 14:55','2017-02-15 14:55',100.00,100.00,'Fechado',1),(20,'2017-02-15 19:08','2017-02-15 23:26',120.00,280.00,'Fechado',1),(21,'2017-02-17 22:09','2017-02-17 22:20',100.00,156.30,'Fechado',1),(22,'2017-02-17 23:12','2017-02-22 19:29',100.00,100.00,'Fechado',1),(23,'2017-02-22 20:05','2017-03-08 01:21',120.00,377.20,'Fechado',1),(24,'2017-03-08 17:13','2017-03-19 16:08',120.00,774.10,'Fechado',1),(25,'2017-03-19 16:13','2017-03-19 17:18',100.00,182.30,'Fechado',1),(26,'2017-03-20 12:32','2017-03-27 18:15',500.00,662.70,'Fechado',1),(27,'2017-03-27 22:57','2017-04-11 11:46',500.00,876.30,'Fechado',1),(28,'2017-04-11 14:13','2017-04-11 15:06',150.00,178.00,'Fechado',1),(29,'2017-04-11 15:07','2017-04-11 15:37',500.00,500.00,'Fechado',1),(30,'2017-04-11 15:37','2017-04-11 16:33',500.00,514.80,'Fechado',1),(31,'2017-04-11 16:34','2017-04-24 16:00',30.00,1274.00,'Fechado',1),(32,'2017-04-24 16:04','2017-04-25 21:58',120.00,678.70,'Fechado',1),(33,'2017-04-25 22:00','2017-04-25 22:19',50.00,171.95,'Fechado',17),(34,'2017-04-25 22:22','2017-04-25 22:39',20.00,91.49,'Fechado',17),(35,'2017-04-25 22:40','2017-04-25 22:45',50.00,139.90,'Fechado',17),(36,'2017-04-25 22:47','2017-04-26 00:35',50.00,171.38,'Fechado',17),(37,'2017-04-26 10:22','2017-04-26 12:14',0.00,71.97,'Fechado',17),(38,'2017-04-26 16:00','2017-04-26 16:08',30.00,30.00,'Fechado',17),(39,'2017-04-26 16:13','2017-04-27 00:34',30.00,796.14,'Fechado',17),(40,'2017-04-27 01:17','2017-04-27 01:19',0.00,0.00,'Fechado',17),(41,'2017-04-27 20:56','2017-04-28 16:01',400.00,874.35,'Fechado',17),(42,'2017-04-28 16:07','2017-04-29 00:21',135.00,1438.37,'Fechado',17),(43,'2017-04-29 07:57','2017-04-29 15:55',53.00,768.84,'Fechado',17),(44,'2017-04-29 15:58','2017-04-29 16:03',35.00,35.00,'Fechado',17),(45,'2017-04-29 16:08','2017-04-30 00:01',35.00,1590.22,'Fechado',17),(46,'2017-04-30 06:53','2017-04-30 08:26',35.00,35.00,'Fechado',17),(47,'2017-04-30 08:27','2017-04-30 15:46',2.00,3.50,'Fechado',17),(48,'2017-04-30 16:01','2017-05-01 01:00',37.00,893.04,'Fechado',17),(49,'2017-05-01 15:34','2017-05-01 15:44',0.00,6.97,'Fechado',17),(50,'2017-05-01 16:34','2017-05-02 00:03',30.00,1197.44,'Fechado',17),(51,'2017-05-02 16:05','2017-05-03 00:14',30.00,464.83,'Fechado',17),(52,'2017-05-03 16:10','2017-05-03 23:36',35.00,336.57,'Fechado',17),(53,'2017-05-03 23:38','2017-05-03 23:38',30.00,30.00,'Fechado',17),(54,'2017-05-03 23:52','2017-05-04 00:06',52.00,793.93,'Fechado',17),(55,'2017-05-04 07:12','2017-05-05 00:32',58.00,806.75,'Fechado',17),(56,'2017-05-05 16:12','2017-05-12 02:03',30.00,1022.98,'Fechado',17),(57,'2017-05-12 02:03','2017-05-22 13:23',120.00,139.99,'Fechado',17),(58,'2017-06-05 16:55','2017-06-16 00:25',120.00,120.00,'Fechado',1),(59,'2017-06-16 00:26','2017-06-16 00:26',100.00,100.00,'Fechado',1),(60,'2017-06-16 11:43','2017-06-22 01:07',120.00,120.00,'Fechado',1),(61,'2017-06-22 10:40','2017-06-22 10:40',110.00,110.00,'Fechado',1),(62,'2017-06-23 16:51','2017-06-24 10:10',120.00,120.00,'Fechado',1),(63,'2017-06-24 10:10','2017-06-24 10:11',20.00,20.00,'Fechado',17),(64,'2017-06-24 10:11','2017-06-24 10:28',0.00,84.86,'Fechado',17),(65,'2017-06-24 10:28','2017-06-24 10:29',20.00,79.97,'Fechado',17),(66,'2017-06-24 10:31','2017-06-24 10:32',50.00,50.00,'Fechado',17),(67,'2017-06-24 10:32','2017-06-24 10:32',20.00,20.00,'Fechado',17),(68,'2017-06-24 10:34','2017-06-24 10:35',10.00,10.00,'Fechado',17),(69,'2017-06-24 10:36','2017-06-24 10:43',0.00,1.00,'Fechado',17),(70,'2017-06-24 10:49','2017-06-24 12:05',0.00,0.00,'Fechado',17),(71,'2017-06-24 12:09','2017-06-24 16:34',170.00,1142.26,'Fechado',17),(72,'2017-06-24 16:35','2017-06-24 23:00',170.00,4105.14,'Fechado',17),(73,'2017-06-24 23:01','2017-06-25 00:32',100.00,457.78,'Fechado',17),(74,'2017-06-25 10:14','2017-06-25 10:23',0.00,45.57,'Fechado',17),(75,'2017-06-25 10:23','2017-06-25 16:06',170.00,817.18,'Fechado',17),(76,'2017-06-25 16:07','2017-06-26 13:55',170.00,3707.59,'Fechado',17),(77,'2017-06-26 13:58','2017-06-26 16:07',170.00,619.19,'Fechado',17),(78,'2017-06-26 16:07','2017-06-30 16:33',170.00,232.88,'Fechado',17),(79,'2017-06-30 16:33','2017-06-30 18:55',120.00,324.29,'Fechado',17),(80,'2017-06-30 18:56','2017-06-30 18:56',20.00,20.00,'Fechado',19),(81,'2017-06-30 21:56','2017-06-30 21:56',50.00,50.00,'Fechado',19),(82,'2017-06-30 23:33','2017-06-30 23:34',22.00,22.00,'Fechado',19),(83,'2017-06-30 23:34','2017-07-01 03:08',55.00,55.00,'Fechado',17),(84,'2017-07-01 20:16','2017-09-09 21:16',100.00,163.29,'Fechado',17),(85,'2017-09-09 21:16','2017-09-14 20:10',120.00,2236.76,'Fechado',17),(86,'2017-09-14 20:11','2017-09-14 20:23',25.00,55.99,'Fechado',17),(87,'2017-09-14 20:24','2017-09-14 21:16',25.00,25.00,'Fechado',17),(88,'2017-09-14 21:20','2017-09-14 21:23',25.00,25.00,'Fechado',17),(89,'2017-09-14 21:28','2017-09-14 21:30',0.00,3.00,'Fechado',17),(90,'2017-09-15 16:26','-',1230.00,0.00,'Aberto',17);
/*!40000 ALTER TABLE `caixa01` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caixa02`
--

DROP TABLE IF EXISTS `caixa02`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caixa02` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caixa02`
--

LOCK TABLES `caixa02` WRITE;
/*!40000 ALTER TABLE `caixa02` DISABLE KEYS */;
INSERT INTO `caixa02` VALUES (1,'0000-00-00 00:00','000-00-00 00:00',0.00,0.00,'Fechado',3),(2,'2017-01-25 00:49','000-00-00 00:00',0.00,0.00,'Fechado',2),(3,'2017-05-22 13:20','000-00-00 00:00',120.00,0.00,'Fechado',1),(4,'2017-06-16 00:22','000-00-00 00:00',100.00,0.00,'Fechado',0),(5,'2017-06-24 10:10','000-00-00 00:00',20.00,0.00,'Fechado',17),(6,'2017-06-29 18:00','000-00-00 00:00',120.00,0.00,'Fechado',19),(7,'2017-06-30 16:34','000-00-00 00:00',0.00,0.00,'Fechado',19),(8,'2017-06-30 23:34','000-00-00 00:00',22.00,0.00,'Fechado',17),(9,'2017-07-01 02:25','000-00-00 00:00',0.00,0.00,'Fechado',19),(10,'2017-07-01 20:16','000-00-00 00:00',0.00,0.00,'Fechado',19);
/*!40000 ALTER TABLE `caixa02` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cargo` varchar(45) NOT NULL,
  `permissao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (1,'admin',1),(2,'Gerente',1),(3,'Operadora',0),(4,'Garçom',0),(5,'Motoboy',0);
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'Pizzas'),(2,'Esfihas'),(3,'Salgados'),(4,'Beirutes'),(5,'Porcoes'),(6,'Bebidas'),(7,'Pasteis'),(8,'Lanches'),(9,'Doces'),(10,'Sorvetes'),(11,'Balas');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8 NOT NULL,
  `cf1` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cf2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `celular` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `taxa_de_entrega` decimal(10,2) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `endereco` varchar(125) CHARACTER SET utf8 DEFAULT NULL,
  `bairro` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `cep` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `delivery` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Cliente Padrão','123','','1122222222','',3.00,'','Rua teste','Teste','01112-234',''),(3,'Junior','188','','1122625640','',2.00,'','Rua doutor valentim bouças','Jardim Tremembé','0',''),(6,'Felipe','54','','123123123','',3.00,'','Rua planalto','jardim palmares','12345-678',''),(7,'Patricia','199','','22222222','',4.00,'','Rua generica','Vila zilda','02323-040',''),(8,'Godofredo','167','','24360912','',4.00,'','Rua tenente amador','Vila Cachoeira','12345060',''),(9,'Carol','120','','213123123','',2.00,'','Rua Lucas Alaman','Jardim Tremembe','123123123',''),(10,'teste','1','','12123123','',0.00,'','teste','','123123123',''),(11,'balcao','50','','10','',0.00,'','rua andorinha','andorinha','023240285',''),(12,'GISELDA/EDU/LIVIA','491','','56797137','',3.00,'','R ENGENHEIRO JORGE OLIVA 491 APTO 162','VILA MASCOTE','04362060','');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(100) DEFAULT NULL,
  `FONE` varchar(15) NOT NULL,
  `CELULAR` varchar(15) NOT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos`
--

DROP TABLE IF EXISTS `contatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `telefone` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos`
--

LOCK TABLES `contatos` WRITE;
/*!40000 ALTER TABLE `contatos` DISABLE KEYS */;
INSERT INTO `contatos` VALUES (1,'Diogo Cezar','xgordo@gmail.com','(43) 3523-2956'),(2,'Mario Sergio','padariajoia@gmail.com','(43) 9915-7944'),(3,'JoÃ£o da Silva','joao@gmail.com','(41) 3453-9876'),(4,'Junior','teste','1231231'),(5,'Felipe','teste12','1241414'),(6,'outro gato','outro@teste','7783894');
/*!40000 ALTER TABLE `contatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpf_nota`
--

DROP TABLE IF EXISTS `cpf_nota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpf_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpf` varchar(11) NOT NULL,
  `origem` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpf_nota`
--

LOCK TABLES `cpf_nota` WRITE;
/*!40000 ALTER TABLE `cpf_nota` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpf_nota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fechamentos`
--

DROP TABLE IF EXISTS `fechamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fechamentos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_caixa` int(11) DEFAULT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fechamentos`
--

LOCK TABLES `fechamentos` WRITE;
/*!40000 ALTER TABLE `fechamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `fechamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forma_pagamento`
--

DROP TABLE IF EXISTS `forma_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forma_pagamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_pagamento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forma_pagamento`
--

LOCK TABLES `forma_pagamento` WRITE;
/*!40000 ALTER TABLE `forma_pagamento` DISABLE KEYS */;
INSERT INTO `forma_pagamento` VALUES (1,'Dinheiro','money icon','din_vendaDAO.php'),(2,'Cartao de Debito','payment icon','vendaDAO.php'),(3,'Cartao de Credito','credit card alternative icon','vendaDAO.php'),(4,'Dinheiro + Debito','',''),(5,'Dinheiro + Credito','','');
/*!40000 ALTER TABLE `forma_pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimentacao_caixa01`
--

DROP TABLE IF EXISTS `movimentacao_caixa01`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimentacao_caixa01` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` decimal(10,2) NOT NULL,
  `tipo_movimentacao` varchar(2) NOT NULL,
  `saldo` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimentacao_caixa01`
--

LOCK TABLES `movimentacao_caixa01` WRITE;
/*!40000 ALTER TABLE `movimentacao_caixa01` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimentacao_caixa01` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nome_nota`
--

DROP TABLE IF EXISTS `nome_nota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nome_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nome_nota`
--

LOCK TABLES `nome_nota` WRITE;
/*!40000 ALTER TABLE `nome_nota` DISABLE KEYS */;
/*!40000 ALTER TABLE `nome_nota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pde_fato_vendas`
--

DROP TABLE IF EXISTS `pde_fato_vendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pde_fato_vendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_venda` varchar(60) NOT NULL,
  `origem_venda` varchar(60) NOT NULL,
  `num_nota_fiscal` int(11) NOT NULL,
  `valor_nota_fiscal` decimal(10,2) DEFAULT NULL,
  `id_forma_pagamento` int(11) NOT NULL,
  `id_abertura` int(11) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `isDezPorc` int(1) DEFAULT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `sat_num_extrato` varchar(20) DEFAULT NULL,
  `sat_num_serie` varchar(20) DEFAULT NULL,
  `sat_data_emissao` varchar(20) DEFAULT NULL,
  `sat_chave_acesso` varchar(100) DEFAULT NULL,
  `sat_xml_fim` longtext,
  `xml_sis` longtext,
  `sat_chave_antiga` varchar(100) DEFAULT NULL,
  `imprimirsn` varchar(3) DEFAULT NULL,
  `sat_file` varchar(255) DEFAULT NULL,
  `sat_file_antigo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pde_fato_vendas`
--

LOCK TABLES `pde_fato_vendas` WRITE;
/*!40000 ALTER TABLE `pde_fato_vendas` DISABLE KEYS */;
INSERT INTO `pde_fato_vendas` VALUES (1,'2017-02-10 03:49','Caixa 01',1,0.00,3,2,'A',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pde_fato_vendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pde_fato_vendas_produtos`
--

DROP TABLE IF EXISTS `pde_fato_vendas_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pde_fato_vendas_produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_nota_fiscal` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pde_fato_vendas_produtos`
--

LOCK TABLES `pde_fato_vendas_produtos` WRITE;
/*!40000 ALTER TABLE `pde_fato_vendas_produtos` DISABLE KEYS */;
INSERT INTO `pde_fato_vendas_produtos` VALUES (1,1,1,1,'');
/*!40000 ALTER TABLE `pde_fato_vendas_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pde_movimentacao`
--

DROP TABLE IF EXISTS `pde_movimentacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pde_movimentacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_movimentacao` varchar(2) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `origem` varchar(45) NOT NULL,
  `id_forma_pagamento` int(11) NOT NULL,
  `num_nota_fiscal` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pde_movimentacao`
--

LOCK TABLES `pde_movimentacao` WRITE;
/*!40000 ALTER TABLE `pde_movimentacao` DISABLE KEYS */;
INSERT INTO `pde_movimentacao` VALUES (1,'E',28.00,'Caixa 01',3,1);
/*!40000 ALTER TABLE `pde_movimentacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa1`
--

DROP TABLE IF EXISTS `pedido_aux_mesa1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa1`
--

LOCK TABLES `pedido_aux_mesa1` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa1` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa10`
--

DROP TABLE IF EXISTS `pedido_aux_mesa10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa10`
--

LOCK TABLES `pedido_aux_mesa10` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa10` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa11`
--

DROP TABLE IF EXISTS `pedido_aux_mesa11`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa11`
--

LOCK TABLES `pedido_aux_mesa11` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa11` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa11` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa12`
--

DROP TABLE IF EXISTS `pedido_aux_mesa12`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa12`
--

LOCK TABLES `pedido_aux_mesa12` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa12` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa12` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa13`
--

DROP TABLE IF EXISTS `pedido_aux_mesa13`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa13` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa13`
--

LOCK TABLES `pedido_aux_mesa13` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa13` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa13` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa14`
--

DROP TABLE IF EXISTS `pedido_aux_mesa14`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa14` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa14`
--

LOCK TABLES `pedido_aux_mesa14` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa14` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa14` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa15`
--

DROP TABLE IF EXISTS `pedido_aux_mesa15`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa15`
--

LOCK TABLES `pedido_aux_mesa15` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa15` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa15` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa16`
--

DROP TABLE IF EXISTS `pedido_aux_mesa16`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa16` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa16`
--

LOCK TABLES `pedido_aux_mesa16` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa16` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa16` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa17`
--

DROP TABLE IF EXISTS `pedido_aux_mesa17`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa17` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa17`
--

LOCK TABLES `pedido_aux_mesa17` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa17` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa17` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa18`
--

DROP TABLE IF EXISTS `pedido_aux_mesa18`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa18` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa18`
--

LOCK TABLES `pedido_aux_mesa18` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa18` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa18` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa19`
--

DROP TABLE IF EXISTS `pedido_aux_mesa19`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa19` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa19`
--

LOCK TABLES `pedido_aux_mesa19` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa19` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa19` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa2`
--

DROP TABLE IF EXISTS `pedido_aux_mesa2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa2`
--

LOCK TABLES `pedido_aux_mesa2` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa2` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa20`
--

DROP TABLE IF EXISTS `pedido_aux_mesa20`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa20` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa20`
--

LOCK TABLES `pedido_aux_mesa20` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa20` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa20` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa3`
--

DROP TABLE IF EXISTS `pedido_aux_mesa3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa3`
--

LOCK TABLES `pedido_aux_mesa3` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa3` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa4`
--

DROP TABLE IF EXISTS `pedido_aux_mesa4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa4`
--

LOCK TABLES `pedido_aux_mesa4` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa4` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa5`
--

DROP TABLE IF EXISTS `pedido_aux_mesa5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa5`
--

LOCK TABLES `pedido_aux_mesa5` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa5` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa6`
--

DROP TABLE IF EXISTS `pedido_aux_mesa6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa6`
--

LOCK TABLES `pedido_aux_mesa6` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa6` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa7`
--

DROP TABLE IF EXISTS `pedido_aux_mesa7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa7`
--

LOCK TABLES `pedido_aux_mesa7` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa7` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa8`
--

DROP TABLE IF EXISTS `pedido_aux_mesa8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa8`
--

LOCK TABLES `pedido_aux_mesa8` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa8` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_aux_mesa9`
--

DROP TABLE IF EXISTS `pedido_aux_mesa9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_aux_mesa9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_aux_mesa9`
--

LOCK TABLES `pedido_aux_mesa9` WRITE;
/*!40000 ALTER TABLE `pedido_aux_mesa9` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_aux_mesa9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_balcao`
--

DROP TABLE IF EXISTS `pedido_balcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_balcao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `impresso` int(11) DEFAULT NULL,
  `senha` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_balcao`
--

LOCK TABLES `pedido_balcao` WRITE;
/*!40000 ALTER TABLE `pedido_balcao` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_balcao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_balcao2`
--

DROP TABLE IF EXISTS `pedido_balcao2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_balcao2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `impresso` int(11) DEFAULT NULL,
  `senha` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_balcao2`
--

LOCK TABLES `pedido_balcao2` WRITE;
/*!40000 ALTER TABLE `pedido_balcao2` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_balcao2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_delivery`
--

DROP TABLE IF EXISTS `pedido_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_motoboy` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_delivery`
--

LOCK TABLES `pedido_delivery` WRITE;
/*!40000 ALTER TABLE `pedido_delivery` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa1`
--

DROP TABLE IF EXISTS `pedido_mesa1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa1`
--

LOCK TABLES `pedido_mesa1` WRITE;
/*!40000 ALTER TABLE `pedido_mesa1` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa10`
--

DROP TABLE IF EXISTS `pedido_mesa10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa10`
--

LOCK TABLES `pedido_mesa10` WRITE;
/*!40000 ALTER TABLE `pedido_mesa10` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa11`
--

DROP TABLE IF EXISTS `pedido_mesa11`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa11`
--

LOCK TABLES `pedido_mesa11` WRITE;
/*!40000 ALTER TABLE `pedido_mesa11` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa11` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa12`
--

DROP TABLE IF EXISTS `pedido_mesa12`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa12`
--

LOCK TABLES `pedido_mesa12` WRITE;
/*!40000 ALTER TABLE `pedido_mesa12` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa12` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa13`
--

DROP TABLE IF EXISTS `pedido_mesa13`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa13` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa13`
--

LOCK TABLES `pedido_mesa13` WRITE;
/*!40000 ALTER TABLE `pedido_mesa13` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa13` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa14`
--

DROP TABLE IF EXISTS `pedido_mesa14`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa14` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa14`
--

LOCK TABLES `pedido_mesa14` WRITE;
/*!40000 ALTER TABLE `pedido_mesa14` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa14` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa15`
--

DROP TABLE IF EXISTS `pedido_mesa15`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa15`
--

LOCK TABLES `pedido_mesa15` WRITE;
/*!40000 ALTER TABLE `pedido_mesa15` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa15` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa16`
--

DROP TABLE IF EXISTS `pedido_mesa16`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa16` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa16`
--

LOCK TABLES `pedido_mesa16` WRITE;
/*!40000 ALTER TABLE `pedido_mesa16` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa16` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa17`
--

DROP TABLE IF EXISTS `pedido_mesa17`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa17` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa17`
--

LOCK TABLES `pedido_mesa17` WRITE;
/*!40000 ALTER TABLE `pedido_mesa17` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa17` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa18`
--

DROP TABLE IF EXISTS `pedido_mesa18`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa18` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa18`
--

LOCK TABLES `pedido_mesa18` WRITE;
/*!40000 ALTER TABLE `pedido_mesa18` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa18` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa19`
--

DROP TABLE IF EXISTS `pedido_mesa19`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa19` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa19`
--

LOCK TABLES `pedido_mesa19` WRITE;
/*!40000 ALTER TABLE `pedido_mesa19` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa19` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa2`
--

DROP TABLE IF EXISTS `pedido_mesa2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa2`
--

LOCK TABLES `pedido_mesa2` WRITE;
/*!40000 ALTER TABLE `pedido_mesa2` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa20`
--

DROP TABLE IF EXISTS `pedido_mesa20`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa20` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa20`
--

LOCK TABLES `pedido_mesa20` WRITE;
/*!40000 ALTER TABLE `pedido_mesa20` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa20` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa3`
--

DROP TABLE IF EXISTS `pedido_mesa3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa3`
--

LOCK TABLES `pedido_mesa3` WRITE;
/*!40000 ALTER TABLE `pedido_mesa3` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa4`
--

DROP TABLE IF EXISTS `pedido_mesa4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa4`
--

LOCK TABLES `pedido_mesa4` WRITE;
/*!40000 ALTER TABLE `pedido_mesa4` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa5`
--

DROP TABLE IF EXISTS `pedido_mesa5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa5`
--

LOCK TABLES `pedido_mesa5` WRITE;
/*!40000 ALTER TABLE `pedido_mesa5` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa6`
--

DROP TABLE IF EXISTS `pedido_mesa6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa6`
--

LOCK TABLES `pedido_mesa6` WRITE;
/*!40000 ALTER TABLE `pedido_mesa6` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa7`
--

DROP TABLE IF EXISTS `pedido_mesa7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa7`
--

LOCK TABLES `pedido_mesa7` WRITE;
/*!40000 ALTER TABLE `pedido_mesa7` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa8`
--

DROP TABLE IF EXISTS `pedido_mesa8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa8`
--

LOCK TABLES `pedido_mesa8` WRITE;
/*!40000 ALTER TABLE `pedido_mesa8` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_mesa9`
--

DROP TABLE IF EXISTS `pedido_mesa9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_mesa9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_mesa9`
--

LOCK TABLES `pedido_mesa9` WRITE;
/*!40000 ALTER TABLE `pedido_mesa9` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_mesa9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos_suspensos`
--

DROP TABLE IF EXISTS `produtos_suspensos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos_suspensos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_suspensao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `id_motoboy` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos_suspensos`
--

LOCK TABLES `produtos_suspensos` WRITE;
/*!40000 ALTER TABLE `produtos_suspensos` DISABLE KEYS */;
/*!40000 ALTER TABLE `produtos_suspensos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retorno_sat`
--

DROP TABLE IF EXISTS `retorno_sat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retorno_sat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retorno_sat` varchar(999) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retorno_sat`
--

LOCK TABLES `retorno_sat` WRITE;
/*!40000 ALTER TABLE `retorno_sat` DISABLE KEYS */;
INSERT INTO `retorno_sat` VALUES (1,'OK: [ENVIO]\r\n'),(2,'Resultado=490002|06000|0000|EMITIDO COM SUCESSO|||PENGZT48aW5mQ0ZlIElkPSJDRmUzNTE3MDk2ODI3NDMwNzAwMDExNDU5MDAwNDQwNjU3MDAwMDA1OTgyMzY3OSIgdmVyc2FvPSIwLjA3IiB2ZXJzYW9EYWRvc0VudD0iMC4wNyIgdmVyc2FvU0I9IjAxMDAwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj45ODIzNjc8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD4wMDA0NDA2NTc8L25zZXJpZVNBVD48bkNGZT4wMDAwMDU8L25DRmU+PGRFbWk+MjAxNzA5MTQ8L2RFbWk+PGhFbWk+MjAyMTQyPC9oRW1pPjxjRFY+OTwvY0RWPjx0cEFtYj4xPC90cEFtYj48Q05QSj4xNzI2OTcyMDAwMDE1MDwvQ05QSj48c2lnbkFDPms0VWdabXlUMWtOY05nc2I5V3ptNFoxajRFcVlsYWFUSnRGNTVCOGFOUktrczg2c3NOOFRFYjZnMC8wUitPMFZ0YTFTU1NmVktmZDJOQzdUVTV0dnRSbWg5TzRuOE43RjRGZEFmNFAvRmhwTTNGcmRTNGVHRDVhbnFmT0oyd3M2L2tEQmpIK1ZOVjVPaTNJL3g4d0liZHJBZ1l4bXpiY0RmNG5GMkNYRU1TcU4vdVM3UXJIbDE3dEYvU1dBYzBoQmphKzlzSkNqS0EzVm9BdEZlNEhiaWRjTVBKektZaUpWWjNpcktvQTdSUi9jWUZ1N2NLeXZlcXI3NnBzazhKRmNVUitXeW1ieVYwM3F0SWVJM3ZmMmZuT3JJd01aUEZML1RxLzV0ZkdBMW1SaFYvSEwvTXlMK2NxTHpLQVRSTjhINHVvNTRLaUhKdmJoZ2RHMmtIUFh5UT09PC9zaWduQUM+PGFzc2luYXR1cmFRUkNPREU+SmozL1dFS2txNGIzbWR2MXNlb'),(3,'HJuODAwRGJZNEdyTFg3c1dBckc4elErVVd5eExwcEtCaUJtME1ZNkZGOTNsRjl5Z3d3RFNQU3MxTWlMSVhyYzJLU3QzQlNBQTZtVUxOd3E5K05mNCtLaWtaTEVyM3d5K2VvTDJRd0svcEpnaklKZDBNc0ZRN3dncVlTbUU2SVVWVWR6eWpBSkRlbE12Z0R2VHNBTkwwa3Z0K1hFWTg2TlduRXdWZ0tkUmJoays0MUZtZlhValF0S01LeWZtakJBVFA0RlBSRFJJRldxcFIySlpUbldXWnJHNlhvelVESWNreHk5emVzeHE0bjhUbmVtdEZCTWUwMXZ3V2JtSGxyWndsbkp1WElTcjVCMGxRPT08L1NpZ25hdHVyZVZhbHVlPjxLZXlJbmZvPjxYNTA5RGF0YT48WDUwOUNlcnRpZmljYXRlPk1JSUdnVENDQkdtZ0F3SUJBZ0lKQVIxcm5jQlp6UHdFTUEwR0NTcUdTSWIzRFFFQkN3VUFNRkV4TlRBekJnTlZCQW9UTEZObFkzSmxkR0Z5YVdFZ1pHRWdSbUY2Wlc1a1lTQmtieUJGYzNSaFpHOGdaR1VnVTJGdklGQmhkV3h2TVJnd0ZnWURWUVFERXc5QlF5QlRRVlFnVTBWR1FWb2dVMUF3SGhjTk1UY3dPVEUwTWpFek5qVXlXaGNOTWpJd09URTBNakV6TmpVeVdqQ0J1akVTTUJBR0ExVUVCUk1KTURBd05EUXdOalUzTVFzd0NRWURWUVFHRXdKQ1VqRVNNQkFHQTFVRUNCTUpVMkZ2SUZCaGRXeHZNUkV3RHdZRFZRUUtFd2hUUlVaQldpMVRVREVQTUEwR0ExVUVDeE1HUVVNdFUwRlVNU2d3SmdZRFZRUUxFeDlCZFhSbGJuUnBZMkZrYnlCd2IzSWdRVklnVTBWR1FWb2dVMUFnVTBGVU1UVXdNd1lEVlFRREV5eEhTVk5GVEVSQklFeFBWVkpFUlZNZ1JFOVRJ'),(4,'numeroSessao=490002\r\n'),(5,'codigoDeRetorno=6000\r\n'),(6,'RetornoStr=490002|06000|0000|EMITIDO COM SUCESSO|||PENGZT48aW5mQ0ZlIElkPSJDRmUzNTE3MDk2ODI3NDMwNzAwMDExNDU5MDAwNDQwNjU3MDAwMDA1OTgyMzY3OSIgdmVyc2FvPSIwLjA3IiB2ZXJzYW9EYWRvc0VudD0iMC4wNyIgdmVyc2FvU0I9IjAxMDAwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj45ODIzNjc8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD4wMDA0NDA2NTc8L25zZXJpZVNBVD48bkNGZT4wMDAwMDU8L25DRmU+PGRFbWk+MjAxNzA5MTQ8L2RFbWk+PGhFbWk+MjAyMTQyPC9oRW1pPjxjRFY+OTwvY0RWPjx0cEFtYj4xPC90cEFtYj48Q05QSj4xNzI2OTcyMDAwMDE1MDwvQ05QSj48c2lnbkFDPms0VWdabXlUMWtOY05nc2I5V3ptNFoxajRFcVlsYWFUSnRGNTVCOGFOUktrczg2c3NOOFRFYjZnMC8wUitPMFZ0YTFTU1NmVktmZDJOQzdUVTV0dnRSbWg5TzRuOE43RjRGZEFmNFAvRmhwTTNGcmRTNGVHRDVhbnFmT0oyd3M2L2tEQmpIK1ZOVjVPaTNJL3g4d0liZHJBZ1l4bXpiY0RmNG5GMkNYRU1TcU4vdVM3UXJIbDE3dEYvU1dBYzBoQmphKzlzSkNqS0EzVm9BdEZlNEhiaWRjTVBKektZaUpWWjNpcktvQTdSUi9jWUZ1N2NLeXZlcXI3NnBzazhKRmNVUitXeW1ieVYwM3F0SWVJM3ZmMmZuT3JJd01aUEZML1RxLzV0ZkdBMW1SaFYvSEwvTXlMK2NxTHpLQVRSTjhINHVvNTRLaUhKdmJoZ2RHMmtIUFh5UT09PC9zaWduQUM+PGFzc2luYXR1cmFRUkNPREU+SmozL1dFS2txNGIzbWR2MXNl'),(7,'dHJuODAwRGJZNEdyTFg3c1dBckc4elErVVd5eExwcEtCaUJtME1ZNkZGOTNsRjl5Z3d3RFNQU3MxTWlMSVhyYzJLU3QzQlNBQTZtVUxOd3E5K05mNCtLaWtaTEVyM3d5K2VvTDJRd0svcEpnaklKZDBNc0ZRN3dncVlTbUU2SVVWVWR6eWpBSkRlbE12Z0R2VHNBTkwwa3Z0K1hFWTg2TlduRXdWZ0tkUmJoays0MUZtZlhValF0S01LeWZtakJBVFA0RlBSRFJJRldxcFIySlpUbldXWnJHNlhvelVESWNreHk5emVzeHE0bjhUbmVtdEZCTWUwMXZ3V2JtSGxyWndsbkp1WElTcjVCMGxRPT08L1NpZ25hdHVyZVZhbHVlPjxLZXlJbmZvPjxYNTA5RGF0YT48WDUwOUNlcnRpZmljYXRlPk1JSUdnVENDQkdtZ0F3SUJBZ0lKQVIxcm5jQlp6UHdFTUEwR0NTcUdTSWIzRFFFQkN3VUFNRkV4TlRBekJnTlZCQW9UTEZObFkzSmxkR0Z5YVdFZ1pHRWdSbUY2Wlc1a1lTQmtieUJGYzNSaFpHOGdaR1VnVTJGdklGQmhkV3h2TVJnd0ZnWURWUVFERXc5QlF5QlRRVlFnVTBWR1FWb2dVMUF3SGhjTk1UY3dPVEUwTWpFek5qVXlXaGNOTWpJd09URTBNakV6TmpVeVdqQ0J1akVTTUJBR0ExVUVCUk1KTURBd05EUXdOalUzTVFzd0NRWURWUVFHRXdKQ1VqRVNNQkFHQTFVRUNCTUpVMkZ2SUZCaGRXeHZNUkV3RHdZRFZRUUtFd2hUUlVaQldpMVRVREVQTUEwR0ExVUVDeE1HUVVNdFUwRlVNU2d3SmdZRFZRUUxFeDlCZFhSbGJuUnBZMkZrYnlCd2IzSWdRVklnVTBWR1FWb2dVMUFnVTBGVU1UVXdNd1lEVlFRREV5eEhTVk5GVEVSQklFeFBWVkpFUlZNZ1JFOVR'),(8,'Arquivo=C:SATVendasAD35170968274307000114590004406570000059823679.xml\r\n'),(9,'XML=<?xml version=\"1.0\" encoding=\"UTF-8\"?><CFe><infCFe Id=\"CFe35170968274307000114590004406570000059823679\" versao=\"0.07\" versaoDadosEnt=\"0.07\" versaoSB=\"010000\"><ide><cUF>35</cUF><cNF>982367</cNF><mod>59</mod><nserieSAT>000440657</nserieSAT><nCFe>000005</nCFe><dEmi>20170914</dEmi><hEmi>202142</hEmi><cDV>9</cDV><tpAmb>1</tpAmb><CNPJ>17269720000150</CNPJ><signAC>k4UgZmyT1kNcNgsb9Wzm4Z1j4EqYlaaTJtF55B8aNRKks86ssN8TEb6g0/0R+O0Vta1SSSfVKfd2NC7TU5tvtRmh9O4n8N7F4FdAf4P/FhpM3FrdS4eGD5anqfOJ2ws6/kDBjH+VNV5Oi3I/x8wIbdrAgYxmzbcDf4nF2CXEMSqN/uS7QrHl17tF/SWAc0hBja+9sJCjKA3VoAtFe4HbidcMPJzKYiJVZ3irKoA7RR/cYFu7cKyveqr76psk8JFcUR+WymbyV03qtIeI3vf2fnOrIwMZPFL/Tq/5tfGA1mRhV/HL/MyL+cqLzKATRN8H4uo54KiHJvbhgdG2kHPXyQ==</signAC><assinaturaQRCODE>Jj3/WEKkq4b3mdv1semzz+uaiA1Ng/4wlDCP3GDsZ13D9lRBoxWedLKjxJEzQP3f9if6fj3gVZrmKVpOE6i1E1o8XslL/7BGf0dRGHutuS0isCoTsAqO208VIIg0dPxixnOjbcBNzUpmT0sMt2Ulrv/PujnkxH02N/R+l3dgOfgYwJqVTKETJMyyg89+18vqQ8ZvGGDsYH9HxbSH3jPmbzr487Vbp56R8XVR+iFrez8F0YgUno9asKwmAyns99NlORdKaoFL'),(10,'kCkz9ndG5xLIH1yEZAhe3fi2Xq4DEOAMG2WnGHwVHa/NzjfDbNoXuihXWEtANC5kUfLExiOch+nF3CvPG1fmUTxi3KKbbMPRilQG02vSkQk7jsqfxgpcFdjtfHCu7pxQ0oRUZDg3ZXrv+f8sbP70CAwEAAaOCAfAwggHsMA4GA1UdDwEB/wQEAwIF4DB1BgNVHSAEbjBsMGoGCSsGAQQBgewtAzBdMFsGCCsGAQUFBwIBFk9odHRwOi8vYWNzYXQuaW1wcmVuc2FvZmljaWFsLmNvbS5ici9yZXBvc2l0b3Jpby9kcGMvYWNzZWZhenNwL2RwY19hY3NlZmF6c3AucGRmMGUGA1UdHwReMFwwWqBYoFaGVGh0dHA6Ly9hY3NhdC5pbXByZW5zYW9maWNpYWwuY29tLmJyL3JlcG9zaXRvcmlvL2xjci9hY3NhdHNlZmF6c3AvYWNzYXRzZWZhenNwY3JsLmNybDCBlAYIKwYBBQUHAQEEgYcwgYQwLgYIKwYBBQUHMAGGImh0dHA6Ly9vY3NwLmltcHJlbnNhb2ZpY2lhbC5jb20uYnIwUgYIKwYBBQUHMAKGRmh0dHA6Ly9hY3NhdC5pbXByZW5zYW9maWNpYWwuY29tLmJyL3JlcG9zaXRvcmlvL2NlcnRpZmljYWRvcy9hY3NhdC5wN2MwEwYDVR0lBAwwCgYIKwYBBQUHAwIwCQYDVR0TBAIwADAkBgNVHREEHTAboBkGBWBMAQMDoBAEDjY4Mjc0MzA3MDAwMTE0MB8GA1UdIwQYMBaAFLCFgbMozXYqKSRLX1hKb2jZrLAqMA0GCSqGSIb3DQEBCwUAA4ICAQBTD6bu+zCia3oDqZBpk6ZYOSOqHfUmEGToA97PRDXz0iJ1uLg/i5Aeo7oQ72YECmn4ChQ6iqDBsW6SSuSd73POCbutygetI3rcrW0gMkM89obR9mVnxD01oFI0Cjhtjcv5COzpAthBhtMY2qdBfFxEQj'),(11,'');
/*!40000 ALTER TABLE `retorno_sat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_config`
--

DROP TABLE IF EXISTS `s_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `versao_layout` varchar(20) DEFAULT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `ie` varchar(12) DEFAULT NULL,
  `signac` text,
  `cnpj_software` varchar(14) DEFAULT NULL,
  `tempo_loop` int(11) DEFAULT NULL,
  `cregtrib` varchar(20) DEFAULT NULL,
  `p_12741` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_config`
--

LOCK TABLES `s_config` WRITE;
/*!40000 ALTER TABLE `s_config` DISABLE KEYS */;
INSERT INTO `s_config` VALUES (1,'0.07','68274307000114','113587869114','k4UgZmyT1kNcNgsb9Wzm4Z1j4EqYlaaTJtF55B8aNRKks86ssN8TEb6g0/0R+O0Vta1SSSfVKfd2NC7TU5tvtRmh9O4n8N7F4FdAf4P/FhpM3FrdS4eGD5anqfOJ2ws6/kDBjH+VNV5Oi3I/x8wIbdrAgYxmzbcDf4nF2CXEMSqN/uS7QrHl17tF/SWAc0hBja+9sJCjKA3VoAtFe4HbidcMPJzKYiJVZ3irKoA7RR/cYFu7cKyveqr76psk8JFcUR+WymbyV03qtIeI3vf2fnOrIwMZPFL/Tq/5tfGA1mRhV/HL/MyL+cqLzKATRN8H4uo54KiHJvbhgdG2kHPXyQ==','17269720000150',60,'1',10);
/*!40000 ALTER TABLE `s_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sabores_pizza`
--

DROP TABLE IF EXISTS `sabores_pizza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sabores_pizza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sabor1` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `sabor2` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `sabor3` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sabores_pizza`
--

LOCK TABLES `sabores_pizza` WRITE;
/*!40000 ALTER TABLE `sabores_pizza` DISABLE KEYS */;
INSERT INTO `sabores_pizza` VALUES (1,'PIZZA MUSSARELA','PIZZA CALABRESA',''),(2,'PIZZA DOIS QUEIJOS','PIZZA MILHO VERDE',''),(3,'ATUM','CALABRESA',''),(4,'MUSSARELA','ATUM','4 QUEIJOS');
/*!40000 ALTER TABLE `sabores_pizza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `senhas`
--

DROP TABLE IF EXISTS `senhas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `senhas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senha` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `senhas`
--

LOCK TABLES `senhas` WRITE;
/*!40000 ALTER TABLE `senhas` DISABLE KEYS */;
INSERT INTO `senhas` VALUES (2,'31038784');
/*!40000 ALTER TABLE `senhas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `senhas_painel`
--

DROP TABLE IF EXISTS `senhas_painel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `senhas_painel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_abertura` int(11) NOT NULL,
  `senha` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `senhas_painel`
--

LOCK TABLES `senhas_painel` WRITE;
/*!40000 ALTER TABLE `senhas_painel` DISABLE KEYS */;
INSERT INTO `senhas_painel` VALUES (1,56,0);
/*!40000 ALTER TABLE `senhas_painel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabela_auxiliar_venda`
--

DROP TABLE IF EXISTS `tabela_auxiliar_venda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabela_auxiliar_venda` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `total` int(100) NOT NULL,
  `pago` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabela_auxiliar_venda`
--

LOCK TABLES `tabela_auxiliar_venda` WRITE;
/*!40000 ALTER TABLE `tabela_auxiliar_venda` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabela_auxiliar_venda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tec_mesas`
--

DROP TABLE IF EXISTS `tec_mesas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tec_mesas` (
  `id` int(11) NOT NULL DEFAULT '0',
  `mesa` varchar(45) CHARACTER SET utf8 NOT NULL,
  `estado` varchar(45) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tec_mesas`
--

LOCK TABLES `tec_mesas` WRITE;
/*!40000 ALTER TABLE `tec_mesas` DISABLE KEYS */;
INSERT INTO `tec_mesas` VALUES (1,'Mesa 01','free'),(2,'Mesa 02','free'),(3,'Mesa 03','free'),(4,'Mesa 04','free'),(5,'Mesa 05','free'),(6,'Mesa 06','free'),(7,'Mesa 07','free'),(8,'Mesa 08','free'),(9,'Mesa 09','free'),(10,'Mesa 10','free'),(11,'Mesa 11','free'),(12,'Mesa 12','free'),(13,'Mesa 13','free'),(14,'Mesa 14','free'),(15,'Mesa 15','free'),(16,'Mesa 16','free'),(17,'Mesa 17','free'),(18,'Mesa 18','free'),(19,'Mesa 19','free'),(20,'Mesa 20','free'),(21,'Mesa 21','free'),(22,'Mesa 22','free'),(23,'Mesa 23','free'),(24,'Mesa 24','free'),(25,'Mesa 25','free'),(26,'Mesa 26','free'),(27,'Mesa 27','free');
/*!40000 ALTER TABLE `tec_mesas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tec_pedido_mesa`
--

DROP TABLE IF EXISTS `tec_pedido_mesa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tec_pedido_mesa` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_produto` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `impresso` int(11) NOT NULL,
  `cozinha` int(11) DEFAULT NULL,
  `foi_pedido` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tec_pedido_mesa`
--

LOCK TABLES `tec_pedido_mesa` WRITE;
/*!40000 ALTER TABLE `tec_pedido_mesa` DISABLE KEYS */;
/*!40000 ALTER TABLE `tec_pedido_mesa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tec_products`
--

DROP TABLE IF EXISTS `tec_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tec_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(10) NOT NULL,
  `name` varchar(65) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `price` decimal(25,2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT 'no_image.png',
  `tax` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT '1',
  `quantity` int(11) DEFAULT '0',
  `barcode_symbology` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'code39',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'standard',
  `details` text CHARACTER SET utf8,
  `alert_quantity` decimal(10,2) DEFAULT '0.00',
  `cozinha` int(11) DEFAULT NULL,
  `origem` int(11) DEFAULT NULL,
  `ncm` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cfop` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icms_grupo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icms_cso` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icms_tributacao` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pis_grupo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pis_cst` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cofins_grupo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cofins_cst` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2053 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tec_products`
--

LOCK TABLES `tec_products` WRITE;
/*!40000 ALTER TABLE `tec_products` DISABLE KEYS */;
INSERT INTO `tec_products` VALUES (751,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',0.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(752,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',1.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(753,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',2.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(754,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',3.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(755,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',4.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(756,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',5.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(757,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',6.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(758,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',7.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(759,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',8.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(760,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',9.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(761,999,'TAXA DE ENTREGA',102,0.00,'no_image.png','0',10.00,0,-1,'','0','',1.00,0,NULL,NULL,'5405',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1797,62,'ABOBRINHA',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1798,1,'ALHO',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1799,2,'ALICHE',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1800,3,'A MODA',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1801,4,'AMERICANA',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1802,5,'ATUM',1,31.00,'no_image.png','0',31.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1803,74,'ATUM ESPECIAL',1,34.00,'no_image.png','0',34.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1804,6,'BACON',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1805,7,'BAIANA',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1806,8,'BAURU',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1807,9,'BERINJELA',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1808,10,'BROCOLIS',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1809,11,'CALABRESA',1,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1810,12,'CARNE SECA',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1811,13,'CATUPIRY',1,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1812,14,'CHAMPIGNON',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1813,15,'CINCO FORMAGGI',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1814,16,'DA MAMA',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1815,17,'DI NAPOLI',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1816,18,'DI PIZZAIOLO I',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1817,75,'DI PIZZAIOLO II',1,37.00,'no_image.png','0',37.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1818,19,'ESCAROLA',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1819,20,'FRANGO',1,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1820,21,'FRANGO CATUPIRY',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1821,22,'GORGONZOLA',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1822,23,'GREGA',1,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1823,24,'JARDINEIRA',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1824,25,'JUNIOR',1,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1825,60,'LIGHT',1,38.00,'no_image.png','0',38.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1826,26,'LOMBO',1,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1827,27,'LOMBO CATUPIRY',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1828,28,'MARGUERITA',1,32.00,'no_image.png','0',32.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1829,29,'MILHO VERDE',1,30.00,'no_image.png','0',30.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1830,30,'MUSSARELA',1,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1831,31,'NAPOLITANA',1,32.00,'no_image.png','0',32.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1832,32,'NOVA CALABRESA',1,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1833,33,'PALMITO',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1834,34,'PEPPERONI',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1835,76,'PERUANA I',1,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1836,35,'PERUANA II',1,39.00,'no_image.png','0',39.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1837,36,'POMODORO',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1838,37,'PORTUGUESA',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1839,38,'PORTUGUESINHA',1,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1840,39,'QUATRO FORMAGGI',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1841,40,'ROMANA',1,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1842,41,'ROMANESCA',1,34.00,'no_image.png','0',34.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1843,42,'RUCULA',1,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1844,43,'SICILIANA',1,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1845,44,'TOSCANA',1,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1846,61,'VEGETARIANA',1,38.00,'no_image.png','0',38.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1847,45,'VENEZA',1,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1848,46,'BANANA',1,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1849,47,'BRIGADEIRO',1,31.00,'no_image.png','0',31.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1850,48,'PRESTIGIO',1,31.00,'no_image.png','0',31.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1851,49,'ROMEU E JULIETA',1,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1852,50,'TRADICIONAL',3,34.00,'no_image.png','0',34.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1853,51,'ATUM',3,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1854,52,'CALABRESA',3,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1855,53,'ESCAROLA',3,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1856,54,'FRANGO',3,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1857,55,'PALMITO',3,34.00,'no_image.png','0',34.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1858,56,'QUATRO FORMAGGI',3,34.00,'no_image.png','0',34.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1859,201,'BORDA CHEDDAR',2,5.00,'no_image.png','0',5.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1860,202,'BORDA CHOCOLATE',2,8.00,'no_image.png','0',8.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1861,203,'BORDA REQUEIJAO',2,4.00,'no_image.png','0',4.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1862,204,'COBERTURA CATUPIRY',2,10.00,'no_image.png','0',10.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1863,205,'MASSA FINA',2,0.00,'no_image.png','0',0.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1864,206,'MASSA GROSSA',2,0.00,'no_image.png','0',0.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1865,207,'MASSA TRADICIONAL',2,0.00,'no_image.png','0',0.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1866,601,'AGUA 510ML',6,3.00,'no_image.png','0',3.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1867,602,'AGUA 1,5L',6,6.00,'no_image.png','0',6.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1868,603,'COCA-COLA 350ML',6,5.00,'no_image.png','0',5.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1869,604,'GUARANA 350ML',6,5.00,'no_image.png','0',5.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1870,605,'KUAT 350ML',6,5.00,'no_image.png','0',5.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1871,606,'GUARANA ZERO 350ML',6,5.00,'no_image.png','0',5.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1872,607,'SKOL 350ML',6,5.00,'no_image.png','0',5.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1873,608,'ITAIPAVA 269ML',6,4.00,'no_image.png','0',4.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1874,609,'GUARANA 2L',6,10.00,'no_image.png','0',10.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1875,610,'FANTA LARANJA 2L',6,9.00,'no_image.png','0',9.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1876,611,'FANTA UVA 2L',6,9.00,'no_image.png','0',9.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1877,612,'SPRITE 2L',6,9.00,'no_image.png','0',9.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1878,613,'KUAT 2L',6,9.00,'no_image.png','0',9.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1879,614,'DOLLY GUARANA 2L',6,6.00,'no_image.png','0',6.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1880,615,'DOLLY LIMAO 2L',6,6.00,'no_image.png','0',6.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1881,616,'ITUBAINA 2L',6,7.00,'no_image.png','0',7.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1882,617,'COCA-COLA ZERO 2L',6,10.00,'no_image.png','0',10.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1883,618,'FANTA GUARANA 2L',6,9.00,'no_image.png','0',9.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1884,619,'SUCO UVA',6,5.00,'no_image.png','0',5.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1885,620,'SUCO MANGA',6,5.00,'no_image.png','0',5.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1886,621,'SUCO MARACUJA',6,5.00,'no_image.png','0',5.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1887,622,'SUCO LARANJA',6,5.00,'no_image.png','0',5.00,0,1,'','1','',0.00,0,0,'','5405','','','','','','',''),(1888,962,'BROTO ABOBRINHA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1889,91,'BROTO ALHO',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1890,92,'BROTO ALICHE',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1891,93,'BROTO A MODA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1892,94,'BROTO AMERICANA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1893,95,'BROTO ATUM',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1894,974,'BROTO ATUM ESPECIAL',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1895,96,'BROTO BACON',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1896,97,'BROTO BAIANA',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1897,98,'BROTO BAURU',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1898,99,'BROTO BERINJELA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1899,910,'BROTO BROCOLIS',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1900,911,'BROTO CALABRESA',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1901,912,'BROTO CARNE SECA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1902,913,'BROTO CATUPIRY',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1903,914,'BROTO CHAMPIGNON',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1904,915,'BROTO CINCO FORMAGGI',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1905,916,'BROTO DA MAMA',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1906,917,'BROTO DI NAPOLI',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1907,918,'BROTO DI PIZZAIOLO I',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1908,975,'BROTO DI PIZZAIOLO II',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1909,919,'BROTO ESCAROLA',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1910,920,'BROTO FRANGO',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1911,921,'BROTO FRANGO CATUPIRY',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1912,922,'BROTO GORGONZOLA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1913,923,'BROTO GREGA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1914,924,'BROTO JARDINEIRA',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1915,925,'BROTO JUNIOR',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1916,960,'BROTO LIGHT',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1917,926,'BROTO LOMBO',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1918,927,'BROTO LOMBO CATUPIRY',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1919,928,'BROTO MARGUERITA',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1920,929,'BROTO MILHO VERDE',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1921,930,'BROTO MUSSARELA',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1922,931,'BROTO NAPOLITANA',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1923,932,'BROTO NOVA CALABRESA',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1924,933,'BROTO PALMITO',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1925,934,'BROTO PEPPERONI',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1926,976,'BROTO PERUANA I',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1927,935,'BROTO PERUANA II',1,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1928,936,'BROTO POMODORO',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1929,937,'BROTO PORTUGUESA',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1930,938,'BROTO PORTUGUESINHA',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1931,939,'BROTO QUATRO FORMAGGI',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1932,940,'BROTO ROMANA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1933,941,'BROTO ROMANESCA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1934,942,'BROTO RUCULA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1935,943,'BROTO SICILIANA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1936,944,'BROTO TOSCANA',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1937,961,'BROTO VEGETARIANA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1938,945,'BROTO VENEZA',1,27.00,'no_image.png','0',27.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1939,946,'BROTO BANANA',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1940,947,'BROTO BRIGADEIRO',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1941,948,'BROTO PRESTIGIO',1,25.00,'no_image.png','0',25.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1942,949,'BROTO ROMEU E JULIETA',1,20.00,'no_image.png','0',20.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1943,9999,'ABOBRINHA',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1944,9999,'ALHO',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1945,9999,'ALICHE',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1946,9999,'A MODA',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1947,9999,'AMERICANA',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1948,9999,'ATUM',98,31.00,'no_image.png','0',31.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1949,9999,'ATUM ESPECIAL',98,34.00,'no_image.png','0',34.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1950,9999,'BACON',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1951,9999,'BAIANA',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1952,9999,'BAURU',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1953,9999,'BERINJELA',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1954,9999,'BROCOLIS',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1955,9999,'CALABRESA',98,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1956,9999,'CARNE SECA',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1957,9999,'CATUPIRY',98,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1958,9999,'CHAMPIGNON',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1959,9999,'CINCO FORMAGGI',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1960,9999,'DA MAMA',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1961,9999,'DI NAPOLI',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1962,9999,'DI PIZZAIOLO I',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1963,9999,'DI PIZZAIOLO II',98,37.00,'no_image.png','0',37.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1964,9999,'ESCAROLA',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1965,9999,'FRANGO',98,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1966,9999,'FRANGO CATUPIRY',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1967,9999,'GORGONZOLA',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1968,9999,'GREGA',98,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1969,9999,'JARDINEIRA',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1970,9999,'JUNIOR',98,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1971,9999,'LIGHT',98,38.00,'no_image.png','0',38.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1972,9999,'LOMBO',98,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1973,9999,'LOMBO CATUPIRY',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1974,9999,'MARGUERITA',98,32.00,'no_image.png','0',32.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1975,9999,'MILHO VERDE',98,30.00,'no_image.png','0',30.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1976,9999,'MUSSARELA',98,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1977,9999,'NAPOLITANA',98,32.00,'no_image.png','0',32.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1978,9999,'NOVA CALABRESA',98,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1979,9999,'PALMITO',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1980,9999,'PEPPERONI',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1981,9999,'PERUANA I',98,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1982,9999,'PERUANA II',98,39.00,'no_image.png','0',39.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1983,9999,'POMODORO',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1984,9999,'PORTUGUESA',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1985,9999,'PORTUGUESINHA',98,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1986,9999,'QUATRO FORMAGGI',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1987,9999,'ROMANA',98,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1988,9999,'ROMANESCA',98,34.00,'no_image.png','0',34.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1989,9999,'RUCULA',98,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1990,9999,'SICILIANA',98,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1991,9999,'TOSCANA',98,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1992,9999,'VEGETARIANA',98,38.00,'no_image.png','0',38.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1993,9999,'VENEZA',98,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1994,9999,'BANANA',98,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1995,9999,'BRIGADEIRO',98,31.00,'no_image.png','0',31.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1996,9999,'PRESTIGIO',98,31.00,'no_image.png','0',31.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1997,9999,'ROMEU E JULIETA',98,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1998,9999,'ABOBRINHA',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(1999,9999,'ALHO',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2000,9999,'ALICHE',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2001,9999,'A MODA',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2002,9999,'AMERICANA',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2003,9999,'ATUM',99,31.00,'no_image.png','0',31.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2004,9999,'ATUM ESPECIAL',99,34.00,'no_image.png','0',34.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2005,9999,'BACON',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2006,9999,'BAIANA',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2007,9999,'BAURU',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2008,9999,'BERINJELA',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2009,9999,'BROCOLIS',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2010,9999,'CALABRESA',99,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2011,9999,'CARNE SECA',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2012,9999,'CATUPIRY',99,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2013,9999,'CHAMPIGNON',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2014,9999,'CINCO FORMAGGI',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2015,9999,'DA MAMA',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2016,9999,'DI NAPOLI',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2017,9999,'DI PIZZAIOLO I',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2018,9999,'DI PIZZAIOLO II',99,37.00,'no_image.png','0',37.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2019,9999,'ESCAROLA',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2020,9999,'FRANGO',99,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2021,9999,'FRANGO CATUPIRY',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2022,9999,'GORGONZOLA',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2023,9999,'GREGA',99,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2024,9999,'JARDINEIRA',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2025,9999,'JUNIOR',99,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2026,9999,'LIGHT',99,38.00,'no_image.png','0',38.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2027,9999,'LOMBO',99,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2028,9999,'LOMBO CATUPIRY',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2029,9999,'MARGUERITA',99,32.00,'no_image.png','0',32.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2030,9999,'MILHO VERDE',99,30.00,'no_image.png','0',30.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2031,9999,'MUSSARELA',99,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2032,9999,'NAPOLITANA',99,32.00,'no_image.png','0',32.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2033,9999,'NOVA CALABRESA',99,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2034,9999,'PALMITO',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2035,9999,'PEPPERONI',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2036,9999,'PERUANA I',99,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2037,9999,'PERUANA II',99,39.00,'no_image.png','0',39.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2038,9999,'POMODORO',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2039,9999,'PORTUGUESA',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2040,9999,'PORTUGUESINHA',99,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2041,9999,'QUATRO FORMAGGI',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2042,9999,'ROMANA',99,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2043,9999,'ROMANESCA',99,34.00,'no_image.png','0',34.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2044,9999,'RUCULA',99,36.00,'no_image.png','0',36.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2045,9999,'SICILIANA',99,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2046,9999,'TOSCANA',99,33.00,'no_image.png','0',33.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2047,9999,'VEGETARIANA',99,38.00,'no_image.png','0',38.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2048,9999,'VENEZA',99,35.00,'no_image.png','0',35.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2049,9999,'BANANA',99,28.00,'no_image.png','0',28.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2050,9999,'BRIGADEIRO',99,31.00,'no_image.png','0',31.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2051,9999,'PRESTIGIO',99,31.00,'no_image.png','0',31.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','',''),(2052,9999,'ROMEU E JULIETA',99,29.00,'no_image.png','0',29.00,0,1,'','0','',0.00,0,0,'','5405','','','','','','','');
/*!40000 ALTER TABLE `tec_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `avatar` varchar(150) NOT NULL,
  `id_cargo` int(11) DEFAULT NULL,
  `ativo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Junior Nascimento','jjunior','ejwkh24','images/boy.png',1,1),(12,'Operadora 1 ','operadora','12345678','images/girl.png',2,0),(14,'Celso','motoboy','12345678','images/boy.png',5,1),(15,'Joaquim','','','images/boy.png',5,1),(16,'-','','','',5,1),(17,'Gerente ','admin','123456','images/boy.png',2,1),(18,'Operador 2 ','caixa2','caixa2','images/boy.png',2,1),(19,'Operador ','balcao','123456','images/boy.png',2,1),(20,'FLAVIO ','admin','123456','images/boy.png',5,1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valores_nota`
--

DROP TABLE IF EXISTS `valores_nota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valores_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `troco` varchar(45) NOT NULL,
  `forma_pagamento` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valores_nota`
--

LOCK TABLES `valores_nota` WRITE;
/*!40000 ALTER TABLE `valores_nota` DISABLE KEYS */;
/*!40000 ALTER TABLE `valores_nota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venda_balcao_hist`
--

DROP TABLE IF EXISTS `venda_balcao_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venda_balcao_hist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_venda` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `data_venda` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `forma_pagamento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_dinheiro` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venda_balcao_hist`
--

LOCK TABLES `venda_balcao_hist` WRITE;
/*!40000 ALTER TABLE `venda_balcao_hist` DISABLE KEYS */;
INSERT INTO `venda_balcao_hist` VALUES (1,1,1,1,'','2017-01-16 15:22','Cartão de Débito',NULL),(2,1,15,1,'','2017-01-16 15:22','Cartão de Débito',NULL),(3,1,5,1,'','2017-01-16 19:28','Dinheiro',NULL),(4,1,17,1,'','2017-01-16 19:28','Dinheiro',NULL),(5,2,3,1,'','2017-01-16 19:59','Dinheiro',NULL),(6,2,4,1,'','2017-01-16 19:59','Dinheiro',NULL),(7,3,5,2,'','2017-01-16 22:45','',NULL),(8,4,12,1,'','2017-01-16 22:47','',NULL),(9,5,10,1,'','2017-01-16 22:48','Cartão de Crédito',NULL),(10,6,1,2,'','2017-01-21 17:26','Dinheiro   Crédito',NULL),(11,7,5,4,'','2017-01-21 17:31','Dinheiro / Débito',NULL),(12,8,5,3,'','2017-01-22 18:36','Dinheiro',NULL),(13,9,8,7,'','2017-01-22 18:47','Dinheiro',NULL),(14,10,1,10,'','2017-01-23 18:41','Dinheiro',NULL),(15,11,2,1,'','2017-01-23 19:21','Dinheiro',NULL),(16,12,5,1,'','2017-01-23 19:22','Dinheiro',NULL);
/*!40000 ALTER TABLE `venda_balcao_hist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendas_history`
--

DROP TABLE IF EXISTS `vendas_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendas_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(10) NOT NULL,
  `Produto` varchar(65) CHARACTER SET utf8 NOT NULL,
  `quantidade` int(11) NOT NULL,
  `Preço` decimal(25,2) DEFAULT NULL,
  `Total` decimal(35,2) DEFAULT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `id_produto` int(11) NOT NULL DEFAULT '0',
  `num_nota_fiscal` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendas_history`
--

LOCK TABLES `vendas_history` WRITE;
/*!40000 ALTER TABLE `vendas_history` DISABLE KEYS */;
INSERT INTO `vendas_history` VALUES (1,1,'ESFIHA CARNE',1,0.99,0.99,'',1,1,NULL),(2,575,'AMERICANA',1,31.00,31.00,'31.00',99,311,NULL),(3,580,'BRASILEIRA',1,30.00,30.00,'',99,316,NULL);
/*!40000 ALTER TABLE `vendas_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendas_motoboys`
--

DROP TABLE IF EXISTS `vendas_motoboys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendas_motoboys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_motoboy` int(11) NOT NULL,
  `entregas` int(11) NOT NULL,
  `total_taxas` decimal(10,2) DEFAULT NULL,
  `id_abertura` int(11) NOT NULL,
  `horario` varchar(45) DEFAULT NULL,
  `pago` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendas_motoboys`
--

LOCK TABLES `vendas_motoboys` WRITE;
/*!40000 ALTER TABLE `vendas_motoboys` DISABLE KEYS */;
INSERT INTO `vendas_motoboys` VALUES (1,14,1,2.00,20,'2017-02-15 21:43',1),(2,14,1,4.00,20,'2017-02-15 21:43',1),(3,15,1,2.00,20,'2017-02-15 21:44',1),(4,15,1,2.00,20,'2017-02-15 21:44',1),(5,15,1,2.00,20,'2017-02-15 22:45',1),(6,15,1,2.00,21,'2017-02-17 22:13',1),(7,14,1,4.00,21,'2017-02-17 22:20',1),(8,0,1,2.00,23,'2017-03-06 22:28',0),(9,0,1,2.00,23,'2017-03-07 00:54',0),(10,0,1,2.00,23,'2017-03-07 01:22',0),(11,0,1,2.00,23,'2017-03-07 01:27',0),(12,0,1,2.00,23,'2017-03-07 18:29',0),(13,15,1,2.00,23,'2017-03-07 22:36',0),(14,0,1,2.00,23,'2017-03-07 22:41',0),(15,15,1,3.00,24,'2017-03-17 00:11',0),(16,14,1,2.00,26,'2017-03-20 13:00',0),(17,14,1,4.00,26,'2017-03-20 22:03',0),(18,0,1,3.00,26,'2017-03-22 18:06',0),(19,0,1,2.00,26,'2017-03-22 18:06',0),(20,15,1,2.00,26,'2017-03-25 17:23',0),(21,14,1,4.00,27,'2017-04-06 22:14',0),(22,15,1,2.00,31,'2017-04-12 21:38',0),(23,0,1,2.00,31,'2017-04-13 00:11',0),(24,15,1,2.00,31,'2017-04-13 00:44',0),(25,15,1,2.00,31,'2017-04-13 00:45',0),(26,14,1,4.00,31,'2017-04-13 00:50',0),(27,14,1,4.00,31,'2017-04-13 01:27',0),(28,14,1,4.00,31,'2017-04-17 15:11',0),(29,0,1,2.00,31,'2017-04-18 00:52',0),(30,15,1,2.00,31,'2017-04-18 00:53',0),(31,14,1,2.00,31,'2017-04-24 15:44',0),(32,14,1,2.00,32,'2017-04-24 22:08',0),(33,16,1,3.00,32,'2017-04-25 07:25',0),(34,16,1,2.00,32,'2017-04-25 07:27',0),(35,16,1,3.00,32,'2017-04-25 07:30',0),(36,14,1,3.00,64,'2017-06-24 10:24',0),(37,16,1,0.00,71,'2017-06-24 12:34',0),(38,16,1,0.00,71,'2017-06-24 12:37',0),(39,16,1,0.00,71,'2017-06-24 12:50',0),(40,16,1,0.00,71,'2017-06-24 12:53',0),(41,16,1,0.00,71,'2017-06-24 12:55',0),(42,16,1,0.00,71,'2017-06-24 12:57',0),(43,16,1,0.00,71,'2017-06-24 13:24',0),(44,16,1,0.00,71,'2017-06-24 13:26',0),(45,16,1,0.00,71,'2017-06-24 13:36',0),(46,16,1,0.00,71,'2017-06-24 14:11',0),(47,16,1,0.00,71,'2017-06-24 14:23',0),(48,16,1,0.00,71,'2017-06-24 14:33',0),(49,16,1,0.00,71,'2017-06-24 15:20',0),(50,16,1,0.00,71,'2017-06-24 15:42',0),(51,16,1,0.00,71,'2017-06-24 16:08',0),(52,16,1,0.00,71,'2017-06-24 16:34',0),(53,16,1,3.00,74,'2017-06-25 10:18',0),(54,16,1,0.00,75,'2017-06-25 14:32',0),(55,16,1,0.00,75,'2017-06-25 14:36',0),(56,16,1,0.00,75,'2017-06-25 15:10',0),(57,16,1,2.00,85,'2017-09-13 20:47',0),(58,16,1,2.00,85,'2017-09-13 22:43',0),(59,14,1,2.00,85,'2017-09-13 22:50',0),(60,16,1,2.00,85,'2017-09-13 22:51',0),(61,16,1,3.00,86,'2017-09-14 20:18',0),(62,16,1,3.00,89,'2017-09-14 21:29',0);
/*!40000 ALTER TABLE `vendas_motoboys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendas_suspensas`
--

DROP TABLE IF EXISTS `vendas_suspensas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendas_suspensas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_suspensao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_suspensao` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendas_suspensas`
--

LOCK TABLES `vendas_suspensas` WRITE;
/*!40000 ALTER TABLE `vendas_suspensas` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendas_suspensas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-15 16:59:33
