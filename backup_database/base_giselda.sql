-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22-Set-2017 às 01:47
-- Versão do servidor: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pdv_cfe`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bkp_produtos`
--

CREATE TABLE `bkp_produtos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` char(255) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `price` decimal(25,2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT 'no_image.png',
  `tax` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT '1',
  `quantity` decimal(15,2) DEFAULT '0.00',
  `barcode_symbology` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'code39',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'standard',
  `details` text CHARACTER SET utf8,
  `alert_quantity` decimal(10,2) DEFAULT '0.00',
  `cozinha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `bkp_produtos`
--

INSERT INTO `bkp_produtos` (`id`, `code`, `name`, `category_id`, `price`, `image`, `tax`, `cost`, `tax_method`, `quantity`, `barcode_symbology`, `type`, `details`, `alert_quantity`, `cozinha`) VALUES
(1, '0001', 'Hamburquer', 2, '2.00', '99ba81363ddbfe5a92c93023e1fd550a.jpg', '0', '4.00', 0, '53.00', 'code39', 'standard', 'Hamburguer com P?o de Hamburguer, queijo, carne, presunto e salada', '5.00', 1),
(2, '0002', 'Mixto Quente', 2, '1.00', '3ba18844e23b27e8224f8fa6b1752208.jpg', '0', '3.00', 0, '8.00', 'code39', 'standard', '', '5.00', 1),
(3, '0003', 'Cahorro Quente', 2, '2.00', '573bc5101fabefd864960416b1752899.jpg', '0', '3.00', 0, '4.00', 'code39', 'standard', '', '5.00', 1),
(4, '0004', 'Bolo de Chocolate', 2, '2.00', '8ad58758122f3a886e859def53da6a6a.jpg', '0', '3.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(5, '0005', 'Coxinha de Frango', 2, '2.00', 'd3115abf501ce492bdf449f72f185fb1.jpg', '0', '3.00', 0, '9.00', 'code39', 'standard', '', '5.00', 0),
(6, '0006', 'Empada', 2, '2.00', '76fed631b7861010869172aa83d78e0a.jpg', '0', '3.00', 0, '19.00', 'code39', 'standard', '', '5.00', 0),
(7, '0007', 'Monteiro Lopes', 2, '2.00', '3274477f5b7d3ef257c4562c56ef387e.jpg', '0', '3.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(8, '0008', 'Risole de Carne', 2, '2.00', '32a3ac97716a9dc68812aecbaf11840a.jpg', '0', '4.00', 0, '4.00', 'code39', 'standard', '', '5.00', 0),
(9, '0009', 'Coxinha de Caranguejo', 2, '4.00', '8bd5b89b645b1bc2d4d08816b5ad3d0b.jpg', '0', '6.00', 0, '6.00', 'code39', 'standard', '', '5.00', 0),
(10, '0010', 'Coxinha de Camar?o', 2, '4.00', '272825062f261b126f1996ed099b4b87.jpg', '0', '6.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(11, '0011', 'Sonho', 2, '2.00', '1f56837339171226e7e33eb0c5e8eae0.jpg', '0', '3.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(12, '0012', 'Lasanha', 2, '6.00', 'fd1c25461a5fbb0597c68bb78100c6ec.jpg', '0', '9.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(13, '0013', 'Torta de Chocolate', 2, '3.00', '11fcdf61a2d8c2d6b7c3e9c0a6996a54.jpg', '0', '6.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(14, '0014', 'Fanta Laranja Lata', 1, '2.00', 'f0ed23add960528f5da95d8fb2a8a106.jpg', '0', '4.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(15, '0015', 'Coca-Cola Lata', 1, '2.00', 'd1ae8344e2fdfc3fcd80a96bb1f00240.jpg', '0', '4.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(16, '0016', '?gua Mineral', 1, '2.00', '91b3bcff369f45e167c3544bad752912.jpg', '0', '3.00', 0, '9.00', 'code39', 'standard', '', '5.00', 0),
(17, '0017', 'Suco de Laranja', 1, '4.00', 'f4cab501731cb47389a6c1a9a54cf736.jpg', '0', '6.00', 0, '5.00', 'code39', 'standard', '', '5.00', 0),
(18, '01', 'Combo M', 2, '10.99', 'no_image.png', '5', '8.71', 0, '0.00', 'code39', 'combo', '', '0.00', 0),
(19, '02', 'Batata M', 2, '7.99', 'no_image.png', '0', '4.72', 0, '0.00', 'code39', 'standard', '', '0.00', 0),
(20, '03', 'Cobertura Cheddar', 2, '1.00', 'no_image.png', '0', '0.27', 0, '0.00', 'code39', 'standard', '', '0.00', 0),
(21, '012', 'Pizza Calabresa', 3, '24.00', 'no_image.png', '0', '12.00', 1, '10.00', 'code39', 'standard', '', '1.00', 1),
(22, '20', 'Pizza Mussarela', 3, '26.00', 'no_image.png', '0', '13.00', 1, '10.00', 'code39', 'standard', '', '0.00', 1),
(23, '027', '1/2 Pizza Mussarela', 3, '26.00', 'no_image.png', '0', '6.00', 0, '11.00', 'code39', 'standard', '', '0.00', 1),
(24, '01777', '1/2 Calabresa', 3, '21.00', 'no_image.png', '0', '6.00', 0, '9.00', 'code39', 'standard', '', '0.00', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `caixa01`
--

CREATE TABLE `caixa01` (
  `id` int(11) NOT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `caixa01`
--

INSERT INTO `caixa01` (`id`, `data_abertura`, `data_fechamento`, `valor_inicial`, `valor_final`, `status`, `id_usuario`) VALUES
(1, '0000-00-00 00:00', '0000-00-00 00:00', '0.00', '0.00', 'Fechado', 1),
(19, '2017-02-15 14:55', '2017-02-15 14:55', '100.00', '100.00', 'Fechado', 1),
(20, '2017-02-15 19:08', '2017-02-15 23:26', '120.00', '280.00', 'Fechado', 1),
(21, '2017-02-17 22:09', '2017-02-17 22:20', '100.00', '156.30', 'Fechado', 1),
(22, '2017-02-17 23:12', '2017-02-22 19:29', '100.00', '100.00', 'Fechado', 1),
(23, '2017-02-22 20:05', '2017-03-08 01:21', '120.00', '377.20', 'Fechado', 1),
(24, '2017-03-08 17:13', '2017-03-19 16:08', '120.00', '774.10', 'Fechado', 1),
(25, '2017-03-19 16:13', '2017-03-19 17:18', '100.00', '182.30', 'Fechado', 1),
(26, '2017-03-20 12:32', '2017-03-27 18:15', '500.00', '662.70', 'Fechado', 1),
(27, '2017-03-27 22:57', '2017-04-11 11:46', '500.00', '876.30', 'Fechado', 1),
(28, '2017-04-11 14:13', '2017-04-11 15:06', '150.00', '178.00', 'Fechado', 1),
(29, '2017-04-11 15:07', '2017-04-11 15:37', '500.00', '500.00', 'Fechado', 1),
(30, '2017-04-11 15:37', '2017-04-11 16:33', '500.00', '514.80', 'Fechado', 1),
(31, '2017-04-11 16:34', '2017-04-24 16:00', '30.00', '1274.00', 'Fechado', 1),
(32, '2017-04-24 16:04', '2017-04-25 21:58', '120.00', '678.70', 'Fechado', 1),
(33, '2017-04-25 22:00', '2017-04-25 22:19', '50.00', '171.95', 'Fechado', 17),
(34, '2017-04-25 22:22', '2017-04-25 22:39', '20.00', '91.49', 'Fechado', 17),
(35, '2017-04-25 22:40', '2017-04-25 22:45', '50.00', '139.90', 'Fechado', 17),
(36, '2017-04-25 22:47', '2017-04-26 00:35', '50.00', '171.38', 'Fechado', 17),
(37, '2017-04-26 10:22', '2017-04-26 12:14', '0.00', '71.97', 'Fechado', 17),
(38, '2017-04-26 16:00', '2017-04-26 16:08', '30.00', '30.00', 'Fechado', 17),
(39, '2017-04-26 16:13', '2017-04-27 00:34', '30.00', '796.14', 'Fechado', 17),
(40, '2017-04-27 01:17', '2017-04-27 01:19', '0.00', '0.00', 'Fechado', 17),
(41, '2017-04-27 20:56', '2017-04-28 16:01', '400.00', '874.35', 'Fechado', 17),
(42, '2017-04-28 16:07', '2017-04-29 00:21', '135.00', '1438.37', 'Fechado', 17),
(43, '2017-04-29 07:57', '2017-04-29 15:55', '53.00', '768.84', 'Fechado', 17),
(44, '2017-04-29 15:58', '2017-04-29 16:03', '35.00', '35.00', 'Fechado', 17),
(45, '2017-04-29 16:08', '2017-04-30 00:01', '35.00', '1590.22', 'Fechado', 17),
(46, '2017-04-30 06:53', '2017-04-30 08:26', '35.00', '35.00', 'Fechado', 17),
(47, '2017-04-30 08:27', '2017-04-30 15:46', '2.00', '3.50', 'Fechado', 17),
(48, '2017-04-30 16:01', '2017-05-01 01:00', '37.00', '893.04', 'Fechado', 17),
(49, '2017-05-01 15:34', '2017-05-01 15:44', '0.00', '6.97', 'Fechado', 17),
(50, '2017-05-01 16:34', '2017-05-02 00:03', '30.00', '1197.44', 'Fechado', 17),
(51, '2017-05-02 16:05', '2017-05-03 00:14', '30.00', '464.83', 'Fechado', 17),
(52, '2017-05-03 16:10', '2017-05-03 23:36', '35.00', '336.57', 'Fechado', 17),
(53, '2017-05-03 23:38', '2017-05-03 23:38', '30.00', '30.00', 'Fechado', 17),
(54, '2017-05-03 23:52', '2017-05-04 00:06', '52.00', '793.93', 'Fechado', 17),
(55, '2017-05-04 07:12', '2017-05-05 00:32', '58.00', '806.75', 'Fechado', 17),
(56, '2017-05-05 16:12', '2017-05-12 02:03', '30.00', '1022.98', 'Fechado', 17),
(57, '2017-05-12 02:03', '2017-05-22 13:23', '120.00', '139.99', 'Fechado', 17),
(58, '2017-06-05 16:55', '2017-06-16 00:25', '120.00', '120.00', 'Fechado', 1),
(59, '2017-06-16 00:26', '2017-06-16 00:26', '100.00', '100.00', 'Fechado', 1),
(60, '2017-06-16 11:43', '2017-06-22 01:07', '120.00', '120.00', 'Fechado', 1),
(61, '2017-06-22 10:40', '2017-06-22 10:40', '110.00', '110.00', 'Fechado', 1),
(62, '2017-06-23 16:51', '2017-06-24 10:10', '120.00', '120.00', 'Fechado', 1),
(63, '2017-06-24 10:10', '2017-06-24 10:11', '20.00', '20.00', 'Fechado', 17),
(64, '2017-06-24 10:11', '2017-06-24 10:28', '0.00', '84.86', 'Fechado', 17),
(65, '2017-06-24 10:28', '2017-06-24 10:29', '20.00', '79.97', 'Fechado', 17),
(66, '2017-06-24 10:31', '2017-06-24 10:32', '50.00', '50.00', 'Fechado', 17),
(67, '2017-06-24 10:32', '2017-06-24 10:32', '20.00', '20.00', 'Fechado', 17),
(68, '2017-06-24 10:34', '2017-06-24 10:35', '10.00', '10.00', 'Fechado', 17),
(69, '2017-06-24 10:36', '2017-06-24 10:43', '0.00', '1.00', 'Fechado', 17),
(70, '2017-06-24 10:49', '2017-06-24 12:05', '0.00', '0.00', 'Fechado', 17),
(71, '2017-06-24 12:09', '2017-06-24 16:34', '170.00', '1142.26', 'Fechado', 17),
(72, '2017-06-24 16:35', '2017-06-24 23:00', '170.00', '4105.14', 'Fechado', 17),
(73, '2017-06-24 23:01', '2017-06-25 00:32', '100.00', '457.78', 'Fechado', 17),
(74, '2017-06-25 10:14', '2017-06-25 10:23', '0.00', '45.57', 'Fechado', 17),
(75, '2017-06-25 10:23', '2017-06-25 16:06', '170.00', '817.18', 'Fechado', 17),
(76, '2017-06-25 16:07', '2017-06-26 13:55', '170.00', '3707.59', 'Fechado', 17),
(77, '2017-06-26 13:58', '2017-06-26 16:07', '170.00', '619.19', 'Fechado', 17),
(78, '2017-06-26 16:07', '2017-06-30 16:33', '170.00', '232.88', 'Fechado', 17),
(79, '2017-06-30 16:33', '2017-06-30 18:55', '120.00', '324.29', 'Fechado', 17),
(80, '2017-06-30 18:56', '2017-06-30 18:56', '20.00', '20.00', 'Fechado', 19),
(81, '2017-06-30 21:56', '2017-06-30 21:56', '50.00', '50.00', 'Fechado', 19),
(82, '2017-06-30 23:33', '2017-06-30 23:34', '22.00', '22.00', 'Fechado', 19),
(83, '2017-06-30 23:34', '2017-07-01 03:08', '55.00', '55.00', 'Fechado', 17),
(84, '2017-07-01 20:16', '2017-09-09 21:16', '100.00', '163.29', 'Fechado', 17),
(85, '2017-09-09 21:16', '2017-09-14 20:10', '120.00', '2236.76', 'Fechado', 17),
(86, '2017-09-14 20:11', '2017-09-14 20:23', '25.00', '55.99', 'Fechado', 17),
(87, '2017-09-14 20:24', '2017-09-14 21:16', '25.00', '25.00', 'Fechado', 17),
(88, '2017-09-14 21:20', '2017-09-14 21:23', '25.00', '25.00', 'Fechado', 17),
(89, '2017-09-14 21:28', '2017-09-14 21:30', '0.00', '3.00', 'Fechado', 17),
(90, '2017-09-15 16:26', '2017-09-15 17:03', '1230.00', '1230.00', 'Fechado', 17),
(91, '2017-09-15 18:11', '2017-09-15 23:24', '0.00', '92.00', 'Fechado', 17),
(92, '2017-09-16 18:35', '2017-09-18 18:15', '0.00', '279.00', 'Fechado', 17),
(93, '2017-09-18 18:17', '2017-09-18 22:56', '12.00', '117.00', 'Fechado', 17),
(94, '2017-09-19 18:18', '2017-09-19 22:50', '0.00', '390.00', 'Fechado', 17),
(95, '2017-09-20 18:13', '2017-09-20 22:55', '0.00', '329.00', 'Fechado', 17),
(96, '2017-09-21 18:11', '-', '0.00', '0.00', 'Aberto', 17);

-- --------------------------------------------------------

--
-- Estrutura da tabela `caixa02`
--

CREATE TABLE `caixa02` (
  `id` int(11) NOT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `caixa02`
--

INSERT INTO `caixa02` (`id`, `data_abertura`, `data_fechamento`, `valor_inicial`, `valor_final`, `status`, `id_usuario`) VALUES
(1, '0000-00-00 00:00', '000-00-00 00:00', '0.00', '0.00', 'Fechado', 3),
(2, '2017-01-25 00:49', '000-00-00 00:00', '0.00', '0.00', 'Fechado', 2),
(3, '2017-05-22 13:20', '000-00-00 00:00', '120.00', '0.00', 'Fechado', 1),
(4, '2017-06-16 00:22', '000-00-00 00:00', '100.00', '0.00', 'Fechado', 0),
(5, '2017-06-24 10:10', '000-00-00 00:00', '20.00', '0.00', 'Fechado', 17),
(6, '2017-06-29 18:00', '000-00-00 00:00', '120.00', '0.00', 'Fechado', 19),
(7, '2017-06-30 16:34', '000-00-00 00:00', '0.00', '0.00', 'Fechado', 19),
(8, '2017-06-30 23:34', '000-00-00 00:00', '22.00', '0.00', 'Fechado', 17),
(9, '2017-07-01 02:25', '000-00-00 00:00', '0.00', '0.00', 'Fechado', 19),
(10, '2017-07-01 20:16', '000-00-00 00:00', '0.00', '0.00', 'Fechado', 19);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `cargo` varchar(45) NOT NULL,
  `permissao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cargo`
--

INSERT INTO `cargo` (`id`, `cargo`, `permissao`) VALUES
(1, 'admin', 1),
(2, 'Gerente', 1),
(3, 'Operadora', 0),
(4, 'Garçom', 0),
(5, 'Motoboy', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `categoria` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`) VALUES
(1, 'Pizzas'),
(2, 'Esfihas'),
(3, 'Salgados'),
(4, 'Beirutes'),
(5, 'Porcoes'),
(6, 'Bebidas'),
(7, 'Pasteis'),
(8, 'Lanches'),
(9, 'Doces'),
(10, 'Sorvetes'),
(11, 'Balas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `name` varchar(55) CHARACTER SET utf8 NOT NULL,
  `cf1` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cf2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `celular` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `taxa_de_entrega` decimal(10,2) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `endereco` varchar(125) CHARACTER SET utf8 DEFAULT NULL,
  `bairro` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `cep` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `delivery` varchar(15) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `name`, `cf1`, `cf2`, `phone`, `celular`, `taxa_de_entrega`, `email`, `endereco`, `bairro`, `cep`, `delivery`) VALUES
(3, 'RITA', '66', '', '55883078', '', '0.00', '', 'RUA 5 DE OUTUBRO', 'AMERICANOPOLIS', '0', ''),
(6, 'ANDRE/DANIELE', '345', '', '55659100', '', '0.00', '', 'R JOSE GASPAR 345 APTO 33', 'AMERICANOPOLIS', '12345-678', ''),
(7, 'CARLOS', '289', '', '972211221', '', '0.00', '', 'R ONOFRE DA SILVEIRA 289 APTO 81', 'AMERICANOPOLIS', '02323-040', ''),
(8, 'ADRIANA DE SOUZA CINTRA', '53', '', '975309052', '', '3.00', '', 'R ARMANDO GOMES DE ARAUJO', 'VILA MARARI', '12345060', ''),
(9, 'ADRIANE OLIVEIRA COSTA', '308', '', '994386022', '', '3.00', '', 'R DAS GRUMIXAMAS 308 AP 83', 'VILA PARQUE JABAQUARA', '123123123', ''),
(10, 'AIRTON HOBO', '246', '', '985384806', '', '3.00', '', 'AV FRANCISCO DE PAULA QUINTANILHA RIBEIRO 246 AP 82/A', 'VILA CAMPESTRE', '123123123', ''),
(11, 'ALAN', '50', '', '983488019', '', '3.00', '', 'CLIENTE TROTE / 8 PEDIDOS EM 30.08', 'VILA MARARI', '023240285', ''),
(12, 'EDU/LIVIA', '491', '', '56797137', '', '3.00', '', 'R ENGENHEIRO JORGE OLIVA 491 APTO 162', 'VILA MASCOTE', '04362060', ''),
(13, 'MARGARIDA', '327', '', '55647950', '', '0.00', '', 'R PROFESSOR ALVARO GUIMARAES FILHO', 'CIDADE ADEMAR', '', ''),
(14, 'ALESSANDRO ROBERTO SERAFIM', '44', '', '954213626', '', '3.00', '', 'R VICOSA DO CEARA 44 APTO 142', 'VILA MASCOTE', '', ''),
(15, 'ALEX', '427', '', '986828639', '', '3.00', '', 'R OCTAVIO  TEIXEIRA MENDES SOBRINHO ', 'VILA SANTA CATARINA', '', ''),
(16, 'ALEX NAZARIO', '151', '', '960708577', '', '3.00', '', 'R RAUL MARQUES MARINHO ', 'VILA INGLESA', '', ''),
(17, 'ALEXANDRE CATTO', '33', '', '27692134', '', '3.00', '', 'R  ARAPA 33 APTO 12/A', 'VILA MASCOTE', '', ''),
(18, 'ALEXANDRE DOS SANTOS DIAS', '208', '', '975150474', '', '3.00', '', 'R DEBORA PASCOAL 208 CASA 4', 'JDIM LOURDES', '', ''),
(19, 'ALEXANDRE MOREIRA', '370', '', '976512119', '', '3.00', '', 'R DR UBALDO FRANCO CAIUBI 370 BL B AP 61', 'VILA SAO PAULO', '', ''),
(20, 'ALLAN GUEDES', '129', '', '40022967', '', '3.00', '', 'R CAMILO CARRERA ', 'VILA CAMPESTRE', '', ''),
(21, 'ANA CRISTINA YOSHIMOCHI', '136', '', '946401901', '', '3.00', '', 'R NESTOR DE CASTRO ', 'CIDADE DOMITILA', '', ''),
(22, 'ANA PAULA DE PAULO', '5', '', '987336406', '', '3.00', '', 'TRAVESSA LUIGI BASSI    CAMPAINHA BRUNO', 'AMERICANOPOLIS', '', ''),
(23, 'ANA PAULA ROSSETTI TASSIN', '43', '', '989794299', '', '3.00', '', 'R SEPINS ', 'AMERICANOPOLIS', '', ''),
(24, 'ANDERSON', '15', '', '966051400', '', '3.00', '', 'R TUPIRITAMA', 'AMERICANOPOLIS', '', ''),
(25, 'ANDERSON VENTURA', '72', '', '981478894', '', '3.00', '', 'R DAMASIO RODRIGUES GOMES', 'JDIM CIDALIA', '', ''),
(26, 'ANDRE', '11', '', '960307210', '', '3.00', '', 'R PEDRO ANDRE ', 'JDIM ORIENTAL      NOTA FALSA $100', '', ''),
(27, 'ANDRE FERNANDO SONEGO', '635', '', '973407040', '', '3.00', '', 'AV MASCOTE 635 AP 81 EDIFICIO SOLEIL', 'VILA MASCOTE', '', ''),
(28, 'ANDREIA DA SILVA VITOR', '220', '', '983753348', '', '3.00', '', 'R DAS GUASSATUNGAS', 'VILA PQUE JABAQUARA', '', ''),
(29, 'AUGUSTO K MARTINS', '4', '', '982767236', '', '3.00', '', 'R SOARES PASSOS ', 'CIDADE DOMITILA', '', ''),
(30, 'RODRIGO CERICOLA', '174', '', '994718782', '', '3.00', '', 'R CLAUDIO MENDONÇA ', 'PQUE JABAQUARA', '', ''),
(31, 'CHRISTINA SAMPAIO', '66', '', '985877331', '', '3.00', '', 'R DERVAL ', 'VILA MASCOTE', '', ''),
(32, 'TELMA/ANDRE/ANDRADE/ALLAN', '118', '', '56795206', '', '0.00', '', 'R PROF ALVARO GUIMARAES FILHO', 'CIDADE ADEMAR', '', ''),
(33, 'MELISSA ALVES', '18', '', '953810838', '', '3.00', '', 'TRAVESSA RIMANCETE', 'AMERICANOPOLIS', '', ''),
(34, 'GRACA', '14', '', '56211270', '', '0.00', '', 'TRAVESSA BARAO DE CAXIAS ', 'CIDADE ADEMAR', '', ''),
(35, '56775928', '21', '', '56775928', '', '0.00', '', 'R FELIPE FOGAÇA DE OLIVEIRA', 'CIDADE DOMITILA', '', ''),
(36, 'RAQUEL ARTAVE', '450', '', '25383323', '', '3.00', '', 'R NELSON FERNANDES 450 BL 3 AP 14', 'CIDADE VARGAS', '', ''),
(37, 'MARIANA GONÇALVES', '85', '', '82269651', '', '3.00', '', 'R NATALINO AMARO TEIXEIRA ', 'VILA FACCHINI', '', ''),
(38, 'PAULO GUILHERME DE CAMPOS', '21', '', '88237455', '', '3.00', '', 'R RANULFO PRATA', 'CIDADE DOMITILA', '', ''),
(39, 'JORGE ANDRADE', '248', '', '994413321', '', '3.00', '', 'R JOAO MARIA SILVEIRA MARQUES', 'JDIM VILAS BOAS', '', ''),
(40, 'JULIANA SILVA', '19', '', '970559285', '', '3.00', '', 'TRAVESSA RIQUEZAS NATURAIS 19 CASA 3', 'AMERICANOPOLIS', '', ''),
(41, 'JESSICA SOOS MOREIRA', '166', '', '81477978', '', '3.00', '', 'R BAQUIRIVU', 'CIDADE ADEMAR', '', ''),
(42, 'TIAGO BORGES', '343', '', '58258257', '', '0.00', '', 'R TUPIRITAMA', 'AMERICANOPOLIS', '', ''),
(43, 'JESSICA SOOS MOREIRA', '166', '', '981477978', '', '3.00', '', 'R BAQUIRIVU', 'CIDADE ADEMAR', '', ''),
(44, 'MARIANA', '490', '', '38921031', '', '0.00', '', 'R DAS ROLINHAS', 'AMERICANOPOLIS', '', ''),
(45, 'FRAN', '625', '', '987312005', '', '0.00', '', 'AV SANTO AFONSO', 'AMERICANOPOLIS', '', ''),
(46, 'OSCAR', '164', '', '945832482', '', '0.00', '', 'R FRANCESCO SOLEMENA', 'CIDADE DOMITILA', '', ''),
(47, 'RODRIGO', '400', '', '961501227', '', '0.00', '', 'R MARCO PALMEZZANO 400 CASA 2', 'AMERICANOPLIS', '', ''),
(48, 'MARLUCE DE PAULA', '175', '', '985516800', '', '3.00', '', 'R GIUSEPPE VERDI 175 AP 65', 'VILA CAMPESTRE', '', ''),
(49, 'JOSELITO/BIANCA', '391', '', '55624600', '', '0.00', '', 'R MARCO PALMEZZANO ', 'AMERICANOPOLIS', '', ''),
(50, 'DANILO', '18', '', '959460843', '', '0.00', '', 'R AUGUSTO ROY', 'JD ZAIRA', '', ''),
(51, 'LUCAS BENTO', '301', '', '947568387', '', '3.00', '', 'R CRUZ DAS ALMAS 301 BL 3 P 131', 'VILA CAMPESTRE', '', ''),
(52, 'GIL/LINA', '232', '', '56774274', '', '0.00', '', 'R COMENDADOR ITALO FRANCESCHI', 'CIDADE DOMITILA', '', ''),
(53, 'RENATA UVARA ANASTACIO BORBA', '1784', '', '999076335', '', '3.00', '', 'AV CUPECE 1784 AP 47/B', 'JDIM PRUDENCIA', '', ''),
(54, 'LEANDRO', '540', '', '43237572', '', '3.00', '', 'R ENGENHEIRO JORGE OLIVA AP 221', 'VILA MASCOTE', '', ''),
(55, 'CAIO/SELMA/FELIPE', '83', '', '986749106', '', '0.00', '', 'R JOAO BETHLEN MOREIRA FILHO', 'VILA CAMPESTRE', '', ''),
(56, 'CINTIA/ZANE', '362', '', '985543803', '', '0.00', '', 'R DR ALCIDES DE CAMPOS', 'AMERICANOPOLIS', '', ''),
(57, 'BRUNA', '601', '', '986891859', '', '0.00', '', 'R PROF RODOLFO DE FREITAS', 'CIDADE ADEMAR', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `ID` int(11) NOT NULL,
  `NOME` varchar(100) DEFAULT NULL,
  `FONE` varchar(15) NOT NULL,
  `CELULAR` varchar(15) NOT NULL,
  `EMAIL` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE `contatos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `telefone` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `nome`, `email`, `telefone`) VALUES
(1, 'Diogo Cezar', 'xgordo@gmail.com', '(43) 3523-2956'),
(2, 'Mario Sergio', 'padariajoia@gmail.com', '(43) 9915-7944'),
(3, 'JoÃ£o da Silva', 'joao@gmail.com', '(41) 3453-9876'),
(4, 'Junior', 'teste', '1231231'),
(5, 'Felipe', 'teste12', '1241414'),
(6, 'outro gato', 'outro@teste', '7783894');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cpf_nota`
--

CREATE TABLE `cpf_nota` (
  `id` int(11) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `origem` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fechamentos`
--

CREATE TABLE `fechamentos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_caixa` int(11) DEFAULT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `forma_pagamento`
--

CREATE TABLE `forma_pagamento` (
  `id` int(11) NOT NULL,
  `forma_pagamento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `forma_pagamento`
--

INSERT INTO `forma_pagamento` (`id`, `forma_pagamento`, `icone`, `link`) VALUES
(1, 'Dinheiro', 'money icon', 'din_vendaDAO.php'),
(2, 'Cartao de Debito', 'payment icon', 'vendaDAO.php'),
(3, 'Cartao de Credito', 'credit card alternative icon', 'vendaDAO.php'),
(4, 'Dinheiro + Debito', '', ''),
(5, 'Dinheiro + Credito', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `movimentacao_caixa01`
--

CREATE TABLE `movimentacao_caixa01` (
  `id` int(11) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `tipo_movimentacao` varchar(2) NOT NULL,
  `saldo` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nome_nota`
--

CREATE TABLE `nome_nota` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pde_fato_vendas`
--

CREATE TABLE `pde_fato_vendas` (
  `id` int(11) NOT NULL,
  `data_venda` varchar(60) NOT NULL,
  `origem_venda` varchar(60) NOT NULL,
  `num_nota_fiscal` int(11) NOT NULL,
  `valor_nota_fiscal` decimal(10,2) DEFAULT NULL,
  `id_forma_pagamento` int(11) NOT NULL,
  `id_abertura` int(11) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `isDezPorc` int(1) DEFAULT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `sat_num_extrato` varchar(20) DEFAULT NULL,
  `sat_num_serie` varchar(20) DEFAULT NULL,
  `sat_data_emissao` varchar(20) DEFAULT NULL,
  `sat_chave_acesso` varchar(100) DEFAULT NULL,
  `sat_xml_fim` longtext,
  `xml_sis` longtext,
  `sat_chave_antiga` varchar(100) DEFAULT NULL,
  `imprimirsn` varchar(3) DEFAULT NULL,
  `sat_file` varchar(255) DEFAULT NULL,
  `sat_file_antigo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pde_fato_vendas`
--

INSERT INTO `pde_fato_vendas` (`id`, `data_venda`, `origem_venda`, `num_nota_fiscal`, `valor_nota_fiscal`, `id_forma_pagamento`, `id_abertura`, `status`, `id_cliente`, `isDezPorc`, `cpf`, `sat_num_extrato`, `sat_num_serie`, `sat_data_emissao`, `sat_chave_acesso`, `sat_xml_fim`, `xml_sis`, `sat_chave_antiga`, `imprimirsn`, `sat_file`, `sat_file_antigo`) VALUES
(1, '2017-02-10 03:49', 'Caixa 01', 1, '0.00', 3, 2, 'A', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '2017-09-15 21:16', 'Delivery', 2, '28.00', 4, 91, 'A', 33, 1, '46390941838', '', '', '', '', '', '', '', '', '', ''),
(3, '2017-09-15 21:35', 'Caixa 01', 3, '3.00', 1, 91, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(4, '2017-09-15 21:57', 'Delivery', 4, '28.00', 1, 91, 'A', 33, 1, '46390941838', '', '', '', '', '', '', '', '', '', ''),
(5, '2017-09-15 22:03', 'Delivery', 5, '33.00', 1, 91, 'A', 34, 1, '', '', '', '', '', '', '', '', '', '', ''),
(6, '2017-09-16 20:10', 'Delivery', 6, '38.00', 2, 92, 'A', 33, 1, '46390941838', '', '', '', '', '', '', '', '', '', ''),
(7, '2017-09-17 18:15', 'Delivery', 7, '69.00', 3, 92, 'A', 35, 1, '', '', '', '', '', '', '', '', '', '', ''),
(8, '2017-09-17 19:16', 'Delivery', 8, '38.00', 3, 92, 'A', 36, 1, '30412885832', '', '', '', '', '', '', '', '', '', ''),
(9, '2017-09-17 19:40', 'Delivery', 9, '39.00', 3, 92, 'A', 37, 1, '31676482873', '', '', '', '', '', '', '', '', '', ''),
(10, '2017-09-17 21:23', 'Delivery', 10, '36.00', 3, 92, 'A', 38, 1, '69441715891', '', '', '', '', '', '', '', '', '', ''),
(11, '2017-09-17 21:57', 'Caixa 01', 11, '59.00', 1, 92, 'A', 0, 1, '16566442820', '', '', '', '', '', '', '', '', '', ''),
(12, '2017-09-18 18:38', 'Caixa 01', 12, '7.00', 1, 93, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(13, '2017-09-18 19:00', 'Caixa 01', 13, '10.00', 1, 93, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(14, '2017-09-18 20:51', 'Delivery', 14, '37.00', 1, 93, 'A', 39, 1, '', '', '', '', '', '', '', '', '', '', ''),
(15, '2017-09-18 21:15', 'Delivery', 15, '51.00', 1, 93, 'A', 40, 1, '', '', '', '', '', '', '', '', '', '', ''),
(16, '2017-09-19 20:29', 'Delivery', 16, '36.00', 1, 94, 'A', 42, 1, '', '', '', '', '', '', '', '', '', '', ''),
(17, '2017-09-19 20:31', 'Delivery', 17, '73.00', 3, 94, 'A', 43, 1, '', '', '', '', '', '', '', '', '', '', ''),
(18, '2017-09-19 20:41', 'Delivery', 18, '48.00', 3, 94, 'A', 44, 1, '', '', '', '', '', '', '', '', '', '', ''),
(19, '2017-09-19 20:44', 'Delivery', 19, '65.00', 1, 94, 'A', 45, 1, '', '', '', '', '', '', '', '', '', '', ''),
(20, '2017-09-19 20:46', 'Caixa 01', 20, '54.00', 2, 94, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(21, '2017-09-19 21:13', 'Caixa 01', 21, '36.00', 2, 94, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(22, '2017-09-19 22:10', 'Delivery', 22, '39.00', 3, 94, 'A', 46, 1, '', '', '', '', '', '', '', '', '', '', ''),
(23, '2017-09-19 22:23', 'Delivery', 23, '39.00', 2, 94, 'A', 46, 1, '', '', '', '', '', '', '', '', '', '', ''),
(24, '2017-09-20 19:56', 'Caixa 01', 24, '61.00', 3, 95, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(25, '2017-09-20 20:04', 'Delivery', 25, '26.00', 1, 95, 'A', 47, 1, '', '', '', '', '', '', '', '', '', '', ''),
(26, '2017-09-20 20:05', 'Caixa 01', 26, '59.00', 2, 95, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(27, '2017-09-20 20:42', 'Delivery', 27, '46.00', 3, 95, 'A', 48, 1, '', '', '', '', '', '', '', '', '', '', ''),
(28, '2017-09-20 20:49', 'Caixa 01', 28, '25.00', 3, 95, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(29, '2017-09-20 21:02', 'Caixa 01', 29, '10.00', 1, 95, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(30, '2017-09-20 21:29', 'Delivery', 30, '33.00', 3, 95, 'A', 49, 1, '', '', '', '', '', '', '', '', '', '', ''),
(31, '2017-09-20 22:48', 'Delivery', 31, '26.00', 3, 95, 'A', 50, 1, '', '', '', '', '', '', '', '', '', '', ''),
(32, '2017-09-20 22:54', 'Delivery', 32, '43.00', 3, 95, 'A', 51, 1, '', '', '', '', '', '', '', '', '', '', ''),
(33, '2017-09-21 19:05', 'Delivery', 33, '32.00', 1, 96, 'A', 52, 1, '', '', '', '', '', '', '', '', '', '', ''),
(34, '2017-09-21 19:08', 'Delivery', 34, '27.00', 3, 96, 'A', 53, 1, '', '', '', '', '', '', '', '', '', '', ''),
(35, '2017-09-21 19:13', 'Caixa 01', 35, '42.00', 3, 96, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(36, '2017-09-21 19:16', 'Delivery', 36, '37.00', 3, 96, 'A', 54, 1, '', '', '', '', '', '', '', '', '', '', ''),
(37, '2017-09-21 19:25', 'Delivery', 37, '43.00', 1, 96, 'A', 55, 1, '', '', '', '', '', '', '', '', '', '', ''),
(38, '2017-09-21 19:44', 'Delivery', 38, '20.00', 1, 96, 'A', 56, 1, '', '', '', '', '', '', '', '', '', '', ''),
(39, '2017-09-21 19:56', 'Caixa 01', 39, '3.50', 1, 96, 'A', 0, 1, '', '', '', '', '', '', '', '', '', '', ''),
(40, '2017-09-21 20:29', 'Delivery', 40, '33.00', 3, 96, 'A', 57, 1, '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pde_fato_vendas_produtos`
--

CREATE TABLE `pde_fato_vendas_produtos` (
  `id` int(11) NOT NULL,
  `num_nota_fiscal` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pde_fato_vendas_produtos`
--

INSERT INTO `pde_fato_vendas_produtos` (`id`, `num_nota_fiscal`, `id_produto`, `quantidade`, `obs`) VALUES
(1, 1, 1, 1, ''),
(2, 2, 754, 1, ''),
(3, 2, 1893, 1, ''),
(4, 3, 1866, 1, ''),
(5, 4, 754, 1, ''),
(6, 4, 2163, 1, ''),
(7, 5, 751, 1, ''),
(8, 5, 1820, 1, ''),
(9, 6, 754, 1, ''),
(10, 6, 2164, 1, ''),
(11, 7, 751, 1, ''),
(12, 7, 2165, 1, ''),
(13, 7, 2166, 1, ''),
(14, 8, 754, 1, ''),
(15, 8, 2167, 1, ''),
(16, 9, 754, 1, ''),
(17, 9, 2168, 1, ''),
(18, 10, 754, 1, ''),
(19, 10, 1840, 1, ''),
(20, 11, 1820, 1, ''),
(21, 11, 1830, 1, ''),
(22, 12, 1873, 2, ''),
(23, 13, 1882, 1, ''),
(24, 14, 754, 1, ''),
(25, 14, 1822, 1, ''),
(26, 15, 754, 1, ''),
(27, 15, 1859, 1, ''),
(28, 15, 2169, 1, ''),
(29, 15, 1882, 1, ''),
(30, 16, 751, 1, ''),
(31, 16, 2173, 1, ''),
(32, 17, 754, 1, ''),
(33, 17, 2170, 1, ''),
(34, 17, 1838, 1, ''),
(35, 18, 751, 1, ''),
(36, 18, 2174, 1, ''),
(37, 18, 1859, 1, ''),
(38, 18, 2171, 1, ''),
(39, 19, 751, 1, ''),
(40, 19, 2175, 1, ''),
(41, 19, 1820, 1, ''),
(42, 20, 1830, 1, ''),
(43, 20, 1809, 1, ''),
(44, 21, 1813, 1, ''),
(45, 22, 751, 1, ''),
(46, 22, 2176, 1, ''),
(47, 22, 2177, 1, ''),
(48, 23, 751, 1, ''),
(49, 23, 2176, 1, ''),
(50, 23, 2177, 1, ''),
(51, 24, 2178, 1, ''),
(52, 24, 2179, 2, ''),
(53, 24, 2180, 1, ''),
(54, 25, 751, 1, ''),
(55, 25, 2189, 1, ''),
(56, 26, 2189, 1, ''),
(57, 26, 2196, 1, ''),
(58, 27, 754, 1, ''),
(59, 27, 2197, 1, ''),
(60, 27, 1881, 1, ''),
(61, 27, 1863, 1, ''),
(62, 28, 2180, 1, ''),
(63, 29, 1872, 2, ''),
(64, 30, 751, 1, ''),
(65, 30, 2198, 1, ''),
(66, 31, 751, 1, ''),
(67, 31, 2189, 1, ''),
(68, 32, 754, 1, ''),
(69, 32, 2199, 1, ''),
(70, 32, 1881, 1, ''),
(71, 33, 751, 1, ''),
(72, 33, 2200, 1, ''),
(73, 34, 754, 1, ''),
(74, 34, 1861, 1, ''),
(75, 34, 2201, 1, ''),
(76, 35, 2202, 1, ''),
(77, 35, 1883, 1, ''),
(78, 36, 754, 1, ''),
(79, 36, 2203, 1, ''),
(80, 37, 751, 1, ''),
(81, 37, 2204, 1, ''),
(82, 37, 2171, 1, ''),
(83, 38, 751, 1, ''),
(84, 38, 1921, 1, ''),
(85, 39, 1873, 1, ''),
(86, 40, 751, 1, ''),
(87, 40, 2205, 1, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pde_movimentacao`
--

CREATE TABLE `pde_movimentacao` (
  `id` int(11) NOT NULL,
  `tipo_movimentacao` varchar(2) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `origem` varchar(45) NOT NULL,
  `id_forma_pagamento` int(11) NOT NULL,
  `num_nota_fiscal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pde_movimentacao`
--

INSERT INTO `pde_movimentacao` (`id`, `tipo_movimentacao`, `valor`, `origem`, `id_forma_pagamento`, `num_nota_fiscal`) VALUES
(1, 'E', '28.00', 'Caixa 01', 3, 1),
(2, 'E', '0.00', 'Delivery', 1, 2),
(3, 'E', '28.00', 'Delivery', 2, 2),
(4, 'E', '5.00', 'Caixa 01', 1, 3),
(5, 'S', '2.00', 'Caixa 01', 1, 3),
(6, 'E', '28.00', 'Delivery', 1, 4),
(7, 'E', '33.00', 'Delivery', 1, 5),
(8, 'E', '38.00', 'Delivery', 2, 6),
(9, 'E', '69.00', 'Delivery', 3, 7),
(10, 'E', '38.00', 'Delivery', 3, 8),
(11, 'E', '39.00', 'Delivery', 3, 9),
(12, 'E', '36.00', 'Delivery', 3, 10),
(13, 'E', '60.00', 'Caixa 01', 1, 11),
(14, 'S', '1.00', 'Caixa 01', 1, 11),
(15, 'E', '7.00', 'Caixa 01', 1, 12),
(16, 'E', '10.00', 'Caixa 01', 1, 13),
(17, 'E', '37.00', 'Delivery', 1, 14),
(18, 'E', '51.00', 'Delivery', 1, 15),
(19, 'E', '50.00', 'Delivery', 1, 16),
(20, 'S', '14.00', 'Delivery', 1, 16),
(21, 'E', '73.00', 'Delivery', 3, 17),
(22, 'E', '48.00', 'Delivery', 3, 18),
(23, 'E', '65.00', 'Delivery', 1, 19),
(24, 'E', '54.00', 'Caixa 01', 2, 20),
(25, 'E', '36.00', 'Caixa 01', 2, 21),
(26, 'E', '39.00', 'Delivery', 3, 22),
(27, 'E', '39.00', 'Delivery', 2, 23),
(28, 'E', '61.00', 'Caixa 01', 3, 24),
(29, 'E', '50.00', 'Delivery', 1, 25),
(30, 'S', '24.00', 'Delivery', 1, 25),
(31, 'E', '59.00', 'Caixa 01', 2, 26),
(32, 'E', '46.00', 'Delivery', 3, 27),
(33, 'E', '25.00', 'Caixa 01', 3, 28),
(34, 'E', '10.00', 'Caixa 01', 1, 29),
(35, 'E', '33.00', 'Delivery', 3, 30),
(36, 'E', '26.00', 'Delivery', 3, 31),
(37, 'E', '43.00', 'Delivery', 3, 32),
(38, 'E', '40.00', 'Delivery', 1, 33),
(39, 'S', '8.00', 'Delivery', 1, 33),
(40, 'E', '27.00', 'Delivery', 3, 34),
(41, 'E', '42.00', 'Caixa 01', 3, 35),
(42, 'E', '37.00', 'Delivery', 3, 36),
(43, 'E', '50.00', 'Delivery', 1, 37),
(44, 'S', '7.00', 'Delivery', 1, 37),
(45, 'E', '20.00', 'Delivery', 1, 38),
(46, 'E', '3.50', 'Caixa 01', 1, 39),
(47, 'E', '33.00', 'Delivery', 3, 40);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa1`
--

CREATE TABLE `pedido_aux_mesa1` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa2`
--

CREATE TABLE `pedido_aux_mesa2` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa3`
--

CREATE TABLE `pedido_aux_mesa3` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa4`
--

CREATE TABLE `pedido_aux_mesa4` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa5`
--

CREATE TABLE `pedido_aux_mesa5` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa6`
--

CREATE TABLE `pedido_aux_mesa6` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa7`
--

CREATE TABLE `pedido_aux_mesa7` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa8`
--

CREATE TABLE `pedido_aux_mesa8` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa9`
--

CREATE TABLE `pedido_aux_mesa9` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa10`
--

CREATE TABLE `pedido_aux_mesa10` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa11`
--

CREATE TABLE `pedido_aux_mesa11` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa12`
--

CREATE TABLE `pedido_aux_mesa12` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa13`
--

CREATE TABLE `pedido_aux_mesa13` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa14`
--

CREATE TABLE `pedido_aux_mesa14` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa15`
--

CREATE TABLE `pedido_aux_mesa15` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa16`
--

CREATE TABLE `pedido_aux_mesa16` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa17`
--

CREATE TABLE `pedido_aux_mesa17` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa18`
--

CREATE TABLE `pedido_aux_mesa18` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa19`
--

CREATE TABLE `pedido_aux_mesa19` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa20`
--

CREATE TABLE `pedido_aux_mesa20` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_balcao`
--

CREATE TABLE `pedido_balcao` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `impresso` int(11) DEFAULT NULL,
  `senha` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_balcao2`
--

CREATE TABLE `pedido_balcao2` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `impresso` int(11) DEFAULT NULL,
  `senha` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_delivery`
--

CREATE TABLE `pedido_delivery` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_motoboy` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `pedido_delivery`
--

INSERT INTO `pedido_delivery` (`id`, `id_produto`, `quantidade`, `obs`, `id_cliente`, `id_motoboy`, `impresso`) VALUES
(1, 751, 1, '', 57, 0, 1),
(2, 2205, 1, '', 57, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa1`
--

CREATE TABLE `pedido_mesa1` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa2`
--

CREATE TABLE `pedido_mesa2` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa3`
--

CREATE TABLE `pedido_mesa3` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa4`
--

CREATE TABLE `pedido_mesa4` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa5`
--

CREATE TABLE `pedido_mesa5` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa6`
--

CREATE TABLE `pedido_mesa6` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa7`
--

CREATE TABLE `pedido_mesa7` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa8`
--

CREATE TABLE `pedido_mesa8` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa9`
--

CREATE TABLE `pedido_mesa9` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa10`
--

CREATE TABLE `pedido_mesa10` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa11`
--

CREATE TABLE `pedido_mesa11` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa12`
--

CREATE TABLE `pedido_mesa12` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa13`
--

CREATE TABLE `pedido_mesa13` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa14`
--

CREATE TABLE `pedido_mesa14` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa15`
--

CREATE TABLE `pedido_mesa15` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa16`
--

CREATE TABLE `pedido_mesa16` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) CHARACTER SET utf8 NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa17`
--

CREATE TABLE `pedido_mesa17` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa18`
--

CREATE TABLE `pedido_mesa18` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa19`
--

CREATE TABLE `pedido_mesa19` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa20`
--

CREATE TABLE `pedido_mesa20` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_suspensos`
--

CREATE TABLE `produtos_suspensos` (
  `id` int(11) NOT NULL,
  `id_suspensao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `id_motoboy` int(11) DEFAULT NULL,
  `impresso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `retorno_sat`
--

CREATE TABLE `retorno_sat` (
  `id` int(11) NOT NULL,
  `retorno_sat` varchar(999) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `retorno_sat`
--

INSERT INTO `retorno_sat` (`id`, `retorno_sat`) VALUES
(1, 'OK: [ENVIO]\r\n'),
(2, 'Resultado=PORTA SERIAL NaO ESTa PRONTA, TENTE NOVAMENTE\r\n'),
(3, 'numeroSessao=0\r\n'),
(4, 'codigoDeRetorno=0\r\n'),
(5, 'RetornoStr=PORTA SERIAL NaO ESTa PRONTA, TENTE NOVAMENTE\r\n'),
(6, 'XML=<?xml version=\"1.0\" encoding=\"UTF-8\"?><CFe><infCFe versaoDadosEnt=\"0.07\"><ide><CNPJ>17269720000150</CNPJ><signAC>k4UgZmyT1kNcNgsb9Wzm4Z1j4EqYlaaTJtF55B8aNRKks86ssN8TEb6g0/0R+O0Vta1SSSfVKfd2NC7TU5tvtRmh9O4n8N7F4FdAf4P/FhpM3FrdS4eGD5anqfOJ2ws6/kDBjH+VNV5Oi3I/x8wIbdrAgYxmzbcDf4nF2CXEMSqN/uS7QrHl17tF/SWAc0hBja+9sJCjKA3VoAtFe4HbidcMPJzKYiJVZ3irKoA7RR/cYFu7cKyveqr76psk8JFcUR+WymbyV03qtIeI3vf2fnOrIwMZPFL/Tq/5tfGA1mRhV/HL/MyL+cqLzKATRN8H4uo54KiHJvbhgdG2kHPXyQ==</signAC><numeroCaixa>001</numeroCaixa></ide><emit><CNPJ>68274307000114</CNPJ><IE>113587869114</IE><indRatISSQN>S</indRatISSQN></emit><dest></dest><det nItem=\"1\"><prod><cProd>0</cProd><xProd>1/2 FRANGO CATUPIRY 1/2 TOSCANA</xProd><CFOP>5405</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>33.00</vUnCom><indRegra>A</indRegra></prod><imposto><vItem12741>0.09</vItem12741><ICMS><ICMSSN102><Orig>0</Orig><CSOSN>500</CSOSN></ICMSSN102></ICMS><PIS><PISSN><CST>49</CST></PISSN></PIS><COFINS><COFINSSN><CST>49</CST></COFINSSN></COFINS></imposto>'),
(7, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sabores_pizza`
--

CREATE TABLE `sabores_pizza` (
  `id` int(11) NOT NULL,
  `sabor1` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `sabor2` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `sabor3` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sabores_pizza`
--

INSERT INTO `sabores_pizza` (`id`, `sabor1`, `sabor2`, `sabor3`) VALUES
(1, 'PIZZA MUSSARELA', 'PIZZA CALABRESA', ''),
(2, 'PIZZA DOIS QUEIJOS', 'PIZZA MILHO VERDE', ''),
(3, 'ATUM', 'CALABRESA', ''),
(4, 'MUSSARELA', 'ATUM', '4 QUEIJOS');

-- --------------------------------------------------------

--
-- Estrutura da tabela `senhas`
--

CREATE TABLE `senhas` (
  `id` int(11) NOT NULL,
  `senha` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `senhas`
--

INSERT INTO `senhas` (`id`, `senha`) VALUES
(2, '31038784');

-- --------------------------------------------------------

--
-- Estrutura da tabela `senhas_painel`
--

CREATE TABLE `senhas_painel` (
  `id` int(11) NOT NULL,
  `id_abertura` int(11) NOT NULL,
  `senha` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `senhas_painel`
--

INSERT INTO `senhas_painel` (`id`, `id_abertura`, `senha`) VALUES
(1, 56, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `s_config`
--

CREATE TABLE `s_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `versao_layout` varchar(20) DEFAULT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `ie` varchar(12) DEFAULT NULL,
  `signac` text,
  `cnpj_software` varchar(14) DEFAULT NULL,
  `tempo_loop` int(11) DEFAULT NULL,
  `cregtrib` varchar(20) DEFAULT NULL,
  `p_12741` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `s_config`
--

INSERT INTO `s_config` (`id`, `versao_layout`, `cnpj`, `ie`, `signac`, `cnpj_software`, `tempo_loop`, `cregtrib`, `p_12741`) VALUES
(1, '0.07', '68274307000114', '113587869114', 'k4UgZmyT1kNcNgsb9Wzm4Z1j4EqYlaaTJtF55B8aNRKks86ssN8TEb6g0/0R+O0Vta1SSSfVKfd2NC7TU5tvtRmh9O4n8N7F4FdAf4P/FhpM3FrdS4eGD5anqfOJ2ws6/kDBjH+VNV5Oi3I/x8wIbdrAgYxmzbcDf4nF2CXEMSqN/uS7QrHl17tF/SWAc0hBja+9sJCjKA3VoAtFe4HbidcMPJzKYiJVZ3irKoA7RR/cYFu7cKyveqr76psk8JFcUR+WymbyV03qtIeI3vf2fnOrIwMZPFL/Tq/5tfGA1mRhV/HL/MyL+cqLzKATRN8H4uo54KiHJvbhgdG2kHPXyQ==', '17269720000150', 60, '1', 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tabela_auxiliar_venda`
--

CREATE TABLE `tabela_auxiliar_venda` (
  `id` int(50) NOT NULL,
  `total` int(100) NOT NULL,
  `pago` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_mesas`
--

CREATE TABLE `tec_mesas` (
  `id` int(11) NOT NULL DEFAULT '0',
  `mesa` varchar(45) CHARACTER SET utf8 NOT NULL,
  `estado` varchar(45) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tec_mesas`
--

INSERT INTO `tec_mesas` (`id`, `mesa`, `estado`) VALUES
(1, 'Mesa 01', 'free'),
(2, 'Mesa 02', 'free'),
(3, 'Mesa 03', 'free'),
(4, 'Mesa 04', 'free'),
(5, 'Mesa 05', 'free'),
(6, 'Mesa 06', 'free'),
(7, 'Mesa 07', 'free'),
(8, 'Mesa 08', 'free'),
(9, 'Mesa 09', 'free'),
(10, 'Mesa 10', 'free'),
(11, 'Mesa 11', 'free'),
(12, 'Mesa 12', 'free'),
(13, 'Mesa 13', 'free'),
(14, 'Mesa 14', 'free'),
(15, 'Mesa 15', 'free'),
(16, 'Mesa 16', 'free'),
(17, 'Mesa 17', 'free'),
(18, 'Mesa 18', 'free'),
(19, 'Mesa 19', 'free'),
(20, 'Mesa 20', 'free'),
(21, 'Mesa 21', 'free'),
(22, 'Mesa 22', 'free'),
(23, 'Mesa 23', 'free'),
(24, 'Mesa 24', 'free'),
(25, 'Mesa 25', 'free'),
(26, 'Mesa 26', 'free'),
(27, 'Mesa 27', 'free');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_pedido_mesa`
--

CREATE TABLE `tec_pedido_mesa` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_produto` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `impresso` int(11) NOT NULL,
  `cozinha` int(11) DEFAULT NULL,
  `foi_pedido` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_products`
--

CREATE TABLE `tec_products` (
  `id` int(11) NOT NULL,
  `code` int(10) NOT NULL,
  `name` varchar(65) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `price` decimal(25,2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT 'no_image.png',
  `tax` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT '1',
  `quantity` int(11) DEFAULT '0',
  `barcode_symbology` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'code39',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'standard',
  `details` text CHARACTER SET utf8,
  `alert_quantity` decimal(10,2) DEFAULT '0.00',
  `cozinha` int(11) DEFAULT NULL,
  `origem` int(11) DEFAULT NULL,
  `ncm` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cfop` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icms_grupo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icms_cso` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icms_tributacao` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pis_grupo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pis_cst` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cofins_grupo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cofins_cst` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tec_products`
--

INSERT INTO `tec_products` (`id`, `code`, `name`, `category_id`, `price`, `image`, `tax`, `cost`, `tax_method`, `quantity`, `barcode_symbology`, `type`, `details`, `alert_quantity`, `cozinha`, `origem`, `ncm`, `cfop`, `icms_grupo`, `icms_cso`, `icms_tributacao`, `pis_grupo`, `pis_cst`, `cofins_grupo`, `cofins_cst`) VALUES
(751, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '0.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(752, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '1.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(753, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '2.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(754, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '3.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(755, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '4.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(756, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '5.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(757, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '6.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(758, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '7.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(759, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '8.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(760, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '9.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(761, 999, 'TAXA DE ENTREGA', 102, '0.00', 'no_image.png', '0', '10.00', 0, -1, '', '0', '', '1.00', 0, NULL, '19023000', '5102', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1797, 62, 'ABOBRINHA', 1, '36.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1798, 1, 'ALHO', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1799, 2, 'ALICHE', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1800, 3, 'A MODA', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1801, 4, 'AMERICANA', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1802, 5, 'ATUM', 1, '31.00', 'no_image.png', '0', '31.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1803, 74, 'ATUM ESPECIAL', 1, '34.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1804, 6, 'BACON', 1, '33.00', 'no_image.png', '0', '32.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1805, 7, 'BAIANA', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1806, 8, 'BAURU', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1807, 9, 'BERINJELA', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1808, 10, 'BROCOLIS', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1809, 11, 'CALABRESA', 1, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1810, 12, 'CARNE SECA', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1811, 13, 'CATUPIRY', 1, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1812, 14, 'CHAMPIGNON', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1813, 15, 'CINCO FORMAGGI', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1814, 16, 'DA MAMA', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1815, 17, 'DI NAPOLI', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1816, 18, 'DI PIZZAIOLO I', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1817, 75, 'PERUANA II', 1, '37.00', 'no_image.png', '0', '38.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1818, 19, 'ESCAROLA', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1819, 20, 'FRANGO', 1, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1820, 21, 'FRANGO CATUPIRY', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1821, 22, 'GORGONZOLA', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1822, 23, 'GREGA', 1, '35.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1823, 24, 'JARDINEIRA', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1824, 25, 'JUNIOR', 1, '35.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1825, 60, 'LIGHT', 1, '38.00', 'no_image.png', '0', '37.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1826, 26, 'LOMBO', 1, '29.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1827, 27, 'LOMBO CATUPIRY', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1828, 28, 'MARGUERITA', 1, '32.00', 'no_image.png', '0', '32.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1829, 29, 'MILHO VERDE', 1, '30.00', 'no_image.png', '0', '29.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1830, 30, 'MUSSARELA', 1, '29.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1831, 31, 'NAPOLITANA', 1, '32.00', 'no_image.png', '0', '31.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1832, 32, 'NOVA CALABRESA', 1, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1833, 33, 'PALMITO', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1834, 34, 'PEPPERONI', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1835, 76, 'DI PIZZAIOLO II', 1, '35.00', 'no_image.png', '0', '39.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1836, 35, 'PERUANA', 1, '39.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1837, 36, 'POMODORO', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1838, 37, 'PORTUGUESA', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1839, 38, 'PORTUGUESINHA', 1, '29.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1840, 39, 'QUATRO FORMAGGI', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1841, 40, 'ROMANA', 1, '35.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1842, 41, 'ROMANESCA', 1, '34.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1843, 42, 'RUCULA', 1, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1844, 43, 'SICILIANA', 1, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1845, 44, 'TOSCANA', 1, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1846, 61, 'VEGETARIANA', 1, '38.00', 'no_image.png', '0', '38.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1847, 45, 'VENEZA', 1, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1848, 46, 'BANANA', 1, '28.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1849, 47, 'BRIGADEIRO', 1, '31.00', 'no_image.png', '0', '30.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1850, 48, 'PRESTIGIO', 1, '31.00', 'no_image.png', '0', '30.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1851, 49, 'ROMEU E JULIETA', 1, '29.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1852, 50, 'CALZONE TRADICIONAL', 3, '34.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1853, 51, 'CALZONE ATUM', 3, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1854, 52, 'CALZONE CALABRESA', 3, '33.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1855, 53, 'CALZONE ESCAROLA', 3, '35.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1856, 54, 'CALZONE FRANGO', 3, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1857, 55, 'CALZONE PALMITO', 3, '34.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1858, 56, 'CALZONE QUATRO FORMAGGI', 3, '34.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1859, 201, 'BORDA CHEDDAR', 2, '5.00', 'no_image.png', '0', '5.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1860, 202, 'BORDA CHOCOLATE', 2, '8.00', 'no_image.png', '0', '8.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1861, 203, 'BORDA REQUEIJAO', 2, '4.00', 'no_image.png', '0', '4.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1862, 204, 'COBERTURA CATUPIRY', 2, '10.00', 'no_image.png', '0', '10.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1863, 205, 'MASSA FINA', 2, '0.00', 'no_image.png', '0', '0.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1864, 206, 'MASSA GROSSA', 2, '0.00', 'no_image.png', '0', '0.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1865, 207, 'MASSA TRADICIONAL', 2, '0.00', 'no_image.png', '0', '0.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1866, 601, 'AGUA 510ML', 6, '3.00', 'no_image.png', '0', '3.00', 0, 0, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1867, 602, 'AGUA 1,5L', 6, '6.00', 'no_image.png', '0', '6.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1868, 603, 'COCA-COLA 350ML', 6, '5.00', 'no_image.png', '0', '5.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1869, 604, 'GUARANA 350ML', 6, '5.00', 'no_image.png', '0', '5.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1870, 605, 'KUAT 350ML', 6, '5.00', 'no_image.png', '0', '5.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1871, 606, 'GUARANA ZERO 350ML', 6, '5.00', 'no_image.png', '0', '5.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1872, 607, 'SKOL 350ML', 6, '5.00', 'no_image.png', '0', '5.00', 0, -1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1873, 608, 'ITAIPAVA 269ML', 6, '4.00', 'no_image.png', '0', '3.50', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1874, 609, 'GUARANA 2L', 6, '10.00', 'no_image.png', '0', '10.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1875, 610, 'FANTA LARANJA 2L', 6, '9.00', 'no_image.png', '0', '9.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1876, 611, 'FANTA UVA 2L', 6, '9.00', 'no_image.png', '0', '9.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1877, 612, 'SPRITE 2L', 6, '9.00', 'no_image.png', '0', '9.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1878, 613, 'KUAT 2L', 6, '9.00', 'no_image.png', '0', '9.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1879, 614, 'DOLLY GUARANA 2L', 6, '6.00', 'no_image.png', '0', '6.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1880, 615, 'DOLLY LIMAO 2L', 6, '6.00', 'no_image.png', '0', '6.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1881, 616, 'ITUBAINA 2L', 6, '7.00', 'no_image.png', '0', '7.00', 0, -1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1882, 617, 'COCA-COLA ZERO 2L', 6, '10.00', 'no_image.png', '0', '10.00', 0, -1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1883, 618, 'FANTA GUARANA 2L', 6, '9.00', 'no_image.png', '0', '9.00', 0, 0, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1884, 619, 'SUCO UVA', 6, '5.00', 'no_image.png', '0', '5.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1885, 620, 'SUCO MANGA', 6, '5.00', 'no_image.png', '0', '5.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1886, 621, 'SUCO MARACUJA', 6, '5.00', 'no_image.png', '0', '5.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1887, 622, 'SUCO LARANJA', 6, '5.00', 'no_image.png', '0', '5.00', 0, 1, '', '1', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1888, 962, 'BROTO ABOBRINHA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1889, 91, 'BROTO ALHO', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1890, 92, 'BROTO ALICHE', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1891, 93, 'BROTO A MODA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1892, 94, 'BROTO AMERICANA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1893, 95, 'BROTO ATUM', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1894, 974, 'BROTO ATUM ESPECIAL', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1895, 96, 'BROTO BACON', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1896, 97, 'BROTO BAIANA', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1897, 98, 'BROTO BAURU', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1898, 99, 'BROTO BERINJELA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1899, 910, 'BROTO BROCOLIS', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1900, 911, 'BROTO CALABRESA', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1901, 912, 'BROTO CARNE SECA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1902, 913, 'BROTO CATUPIRY', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1903, 914, 'BROTO CHAMPIGNON', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1904, 915, 'BROTO CINCO FORMAGGI', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1905, 916, 'BROTO DA MAMA', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1906, 917, 'BROTO DI NAPOLI', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1907, 918, 'BROTO DI PIZZAIOLO I', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1908, 975, 'BROTO DI PIZZAIOLO II', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1909, 919, 'BROTO ESCAROLA', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1910, 920, 'BROTO FRANGO', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1911, 921, 'BROTO FRANGO CATUPIRY', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1912, 922, 'BROTO GORGONZOLA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1913, 923, 'BROTO GREGA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1914, 924, 'BROTO JARDINEIRA', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1915, 925, 'BROTO JUNIOR', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1916, 960, 'BROTO LIGHT', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1917, 926, 'BROTO LOMBO', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1918, 927, 'BROTO LOMBO CATUPIRY', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1919, 928, 'BROTO MARGUERITA', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1920, 929, 'BROTO MILHO VERDE', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1921, 930, 'BROTO MUSSARELA', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1922, 931, 'BROTO NAPOLITANA', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1923, 932, 'BROTO NOVA CALABRESA', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1924, 933, 'BROTO PALMITO', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1925, 934, 'BROTO PEPPERONI', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1926, 976, 'BROTO PERUANA I', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1927, 935, 'BROTO PERUANA II', 1, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1928, 936, 'BROTO POMODORO', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1929, 937, 'BROTO PORTUGUESA', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1930, 938, 'BROTO PORTUGUESINHA', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1931, 939, 'BROTO QUATRO FORMAGGI', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1932, 940, 'BROTO ROMANA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1933, 941, 'BROTO ROMANESCA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1934, 942, 'BROTO RUCULA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1935, 943, 'BROTO SICILIANA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1936, 944, 'BROTO TOSCANA', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1937, 961, 'BROTO VEGETARIANA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1938, 945, 'BROTO VENEZA', 1, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1939, 946, 'BROTO BANANA', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1940, 947, 'BROTO BRIGADEIRO', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1941, 948, 'BROTO PRESTIGIO', 1, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1942, 949, 'BROTO ROMEU E JULIETA', 1, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1943, 9999, 'ABOBRINHA', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1944, 9999, 'ALHO', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1945, 9999, 'ALICHE', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1946, 9999, 'A MODA', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1947, 9999, 'AMERICANA', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1948, 9999, 'ATUM', 98, '31.00', 'no_image.png', '0', '31.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1949, 9999, 'ATUM ESPECIAL', 98, '34.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1950, 9999, 'BACON', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1951, 9999, 'BAIANA', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1952, 9999, 'BAURU', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1953, 9999, 'BERINJELA', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1954, 9999, 'BROCOLIS', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1955, 9999, 'CALABRESA', 98, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1956, 9999, 'CARNE SECA', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1957, 9999, 'CATUPIRY', 98, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1958, 9999, 'CHAMPIGNON', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1959, 9999, 'CINCO FORMAGGI', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1960, 9999, 'DA MAMA', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1961, 9999, 'DI NAPOLI', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1962, 9999, 'DI PIZZAIOLO I', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1963, 9999, 'DI PIZZAIOLO II', 98, '37.00', 'no_image.png', '0', '37.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1964, 9999, 'ESCAROLA', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1965, 9999, 'FRANGO', 98, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1966, 9999, 'FRANGO CATUPIRY', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1967, 9999, 'GORGONZOLA', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1968, 9999, 'GREGA', 98, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1969, 9999, 'JARDINEIRA', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1970, 9999, 'JUNIOR', 98, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1971, 9999, 'LIGHT', 98, '38.00', 'no_image.png', '0', '38.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1972, 9999, 'LOMBO', 98, '29.00', 'no_image.png', '0', '29.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1973, 9999, 'LOMBO CATUPIRY', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1974, 9999, 'MARGUERITA', 98, '32.00', 'no_image.png', '0', '32.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1975, 9999, 'MILHO VERDE', 98, '30.00', 'no_image.png', '0', '30.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1976, 9999, 'MUSSARELA', 98, '29.00', 'no_image.png', '0', '29.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1977, 9999, 'NAPOLITANA', 98, '32.00', 'no_image.png', '0', '32.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1978, 9999, 'NOVA CALABRESA', 98, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1979, 9999, 'PALMITO', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1980, 9999, 'PEPPERONI', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1981, 9999, 'PERUANA I', 98, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1982, 9999, 'PERUANA II', 98, '39.00', 'no_image.png', '0', '39.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1983, 9999, 'POMODORO', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1984, 9999, 'PORTUGUESA', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1985, 9999, 'PORTUGUESINHA', 98, '29.00', 'no_image.png', '0', '29.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1986, 9999, 'QUATRO FORMAGGI', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1987, 9999, 'ROMANA', 98, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1988, 9999, 'ROMANESCA', 98, '34.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1989, 9999, 'RUCULA', 98, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1990, 9999, 'SICILIANA', 98, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1991, 9999, 'TOSCANA', 98, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1992, 9999, 'VEGETARIANA', 98, '38.00', 'no_image.png', '0', '38.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1993, 9999, 'VENEZA', 98, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1994, 9999, 'BANANA', 98, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1995, 9999, 'BRIGADEIRO', 98, '31.00', 'no_image.png', '0', '31.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1996, 9999, 'PRESTIGIO', 98, '31.00', 'no_image.png', '0', '31.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1997, 9999, 'ROMEU E JULIETA', 98, '29.00', 'no_image.png', '0', '29.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1998, 9999, 'ABOBRINHA', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(1999, 9999, 'ALHO', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2000, 9999, 'ALICHE', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2001, 9999, 'A MODA', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2002, 9999, 'AMERICANA', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2003, 9999, 'ATUM', 99, '31.00', 'no_image.png', '0', '31.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2004, 9999, 'ATUM ESPECIAL', 99, '34.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2005, 9999, 'BACON', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2006, 9999, 'BAIANA', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2007, 9999, 'BAURU', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2008, 9999, 'BERINJELA', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2009, 9999, 'BROCOLIS', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2010, 9999, 'CALABRESA', 99, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2011, 9999, 'CARNE SECA', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2012, 9999, 'CATUPIRY', 99, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2013, 9999, 'CHAMPIGNON', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2014, 9999, 'CINCO FORMAGGI', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2015, 9999, 'DA MAMA', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2016, 9999, 'DI NAPOLI', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2017, 9999, 'DI PIZZAIOLO I', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2018, 9999, 'DI PIZZAIOLO II', 99, '37.00', 'no_image.png', '0', '37.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2019, 9999, 'ESCAROLA', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2020, 9999, 'FRANGO', 99, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2021, 9999, 'FRANGO CATUPIRY', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2022, 9999, 'GORGONZOLA', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2023, 9999, 'GREGA', 99, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2024, 9999, 'JARDINEIRA', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2025, 9999, 'JUNIOR', 99, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2026, 9999, 'LIGHT', 99, '38.00', 'no_image.png', '0', '38.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2027, 9999, 'LOMBO', 99, '29.00', 'no_image.png', '0', '29.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2028, 9999, 'LOMBO CATUPIRY', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2029, 9999, 'MARGUERITA', 99, '32.00', 'no_image.png', '0', '32.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2030, 9999, 'MILHO VERDE', 99, '30.00', 'no_image.png', '0', '30.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2031, 9999, 'MUSSARELA', 99, '29.00', 'no_image.png', '0', '29.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2032, 9999, 'NAPOLITANA', 99, '32.00', 'no_image.png', '0', '32.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2033, 9999, 'NOVA CALABRESA', 99, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2034, 9999, 'PALMITO', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2035, 9999, 'PEPPERONI', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2036, 9999, 'PERUANA I', 99, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2037, 9999, 'PERUANA II', 99, '39.00', 'no_image.png', '0', '39.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2038, 9999, 'POMODORO', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2039, 9999, 'PORTUGUESA', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2040, 9999, 'PORTUGUESINHA', 99, '29.00', 'no_image.png', '0', '29.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2041, 9999, 'QUATRO FORMAGGI', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2042, 9999, 'ROMANA', 99, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2043, 9999, 'ROMANESCA', 99, '34.00', 'no_image.png', '0', '34.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2044, 9999, 'RUCULA', 99, '36.00', 'no_image.png', '0', '36.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2045, 9999, 'SICILIANA', 99, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2046, 9999, 'TOSCANA', 99, '33.00', 'no_image.png', '0', '33.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2047, 9999, 'VEGETARIANA', 99, '38.00', 'no_image.png', '0', '38.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2048, 9999, 'VENEZA', 99, '35.00', 'no_image.png', '0', '35.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2049, 9999, 'BANANA', 99, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2050, 9999, 'BRIGADEIRO', 99, '31.00', 'no_image.png', '0', '31.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2051, 9999, 'PRESTIGIO', 99, '31.00', 'no_image.png', '0', '31.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2052, 9999, 'ROMEU E JULIETA', 99, '29.00', 'no_image.png', '0', '29.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2053, 9999, 'BROTO ABOBRINHA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2054, 9999, 'BROTO ALHO', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2055, 9999, 'BROTO ALICHE', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2056, 9999, 'BROTO A MODA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2057, 9999, 'BROTO AMERICANA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2058, 9999, 'BROTO ATUM', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2059, 9999, 'BROTO ATUM ESPECIAL', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2060, 9999, 'BROTO BACON', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2061, 9999, 'BROTO BAIANA', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2062, 9999, 'BROTO BAURU', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2063, 9999, 'BROTO BERINJELA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2064, 9999, 'BROTO BROCOLIS', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2065, 9999, 'BROTO CALABRESA', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2066, 9999, 'BROTO CARNE SECA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2067, 9999, 'BROTO CATUPIRY', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2068, 9999, 'BROTO CHAMPIGNON', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2069, 9999, 'BROTO CINCO FORMAGGI', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2070, 9999, 'BROTO DA MAMA', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2071, 9999, 'BROTO DI NAPOLI', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2072, 9999, 'BROTO DI PIZZAIOLO I', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2073, 9999, 'BROTO DI PIZZAIOLO II', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2074, 9999, 'BROTO ESCAROLA', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2075, 9999, 'BROTO FRANGO', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2076, 9999, 'BROTO FRANGO CATUPIRY', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2077, 9999, 'BROTO GORGONZOLA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2078, 9999, 'BROTO GREGA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2079, 9999, 'BROTO JARDINEIRA', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2080, 9999, 'BROTO JUNIOR', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2081, 9999, 'BROTO LIGHT', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2082, 9999, 'BROTO LOMBO', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2083, 9999, 'BROTO LOMBO CATUPIRY', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2084, 9999, 'BROTO MARGUERITA', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2085, 9999, 'BROTO MILHO VERDE', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2086, 9999, 'BROTO MUSSARELA', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2087, 9999, 'BROTO NAPOLITANA', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2088, 9999, 'BROTO NOVA CALABRESA', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2089, 9999, 'BROTO PALMITO', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2090, 9999, 'BROTO PEPPERONI', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2091, 9999, 'BROTO PERUANA I', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2092, 9999, 'BROTO PERUANA II', 98, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2093, 9999, 'BROTO POMODORO', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2094, 9999, 'BROTO PORTUGUESA', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2095, 9999, 'BROTO PORTUGUESINHA', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2096, 9999, 'BROTO QUATRO FORMAGGI', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2097, 9999, 'BROTO ROMANA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2098, 9999, 'BROTO ROMANESCA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2099, 9999, 'BROTO RUCULA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2100, 9999, 'BROTO SICILIANA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2101, 9999, 'BROTO TOSCANA', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2102, 9999, 'BROTO VEGETARIANA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2103, 9999, 'BROTO VENEZA', 98, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2104, 9999, 'BROTO BANANA', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2105, 9999, 'BROTO BRIGADEIRO', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2106, 9999, 'BROTO PRESTIGIO', 98, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2107, 9999, 'BROTO ROMEU E JULIETA', 98, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2108, 9999, 'BROTO ABOBRINHA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2109, 9999, 'BROTO ALHO', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2110, 9999, 'BROTO ALICHE', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2111, 9999, 'BROTO A MODA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2112, 9999, 'BROTO AMERICANA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2113, 9999, 'BROTO ATUM', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2114, 9999, 'BROTO ATUM ESPECIAL', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', '');
INSERT INTO `tec_products` (`id`, `code`, `name`, `category_id`, `price`, `image`, `tax`, `cost`, `tax_method`, `quantity`, `barcode_symbology`, `type`, `details`, `alert_quantity`, `cozinha`, `origem`, `ncm`, `cfop`, `icms_grupo`, `icms_cso`, `icms_tributacao`, `pis_grupo`, `pis_cst`, `cofins_grupo`, `cofins_cst`) VALUES
(2115, 9999, 'BROTO BACON', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2116, 9999, 'BROTO BAIANA', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2117, 9999, 'BROTO BAURU', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2118, 9999, 'BROTO BERINJELA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2119, 9999, 'BROTO BROCOLIS', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2120, 9999, 'BROTO CALABRESA', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2121, 9999, 'BROTO CARNE SECA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2122, 9999, 'BROTO CATUPIRY', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2123, 9999, 'BROTO CHAMPIGNON', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2124, 9999, 'BROTO CINCO FORMAGGI', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2125, 9999, 'BROTO DA MAMA', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2126, 9999, 'BROTO DI NAPOLI', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2127, 9999, 'BROTO DI PIZZAIOLO I', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2128, 9999, 'BROTO DI PIZZAIOLO II', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2129, 9999, 'BROTO ESCAROLA', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2130, 9999, 'BROTO FRANGO', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2131, 9999, 'BROTO FRANGO CATUPIRY', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2132, 9999, 'BROTO GORGONZOLA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2133, 9999, 'BROTO GREGA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2134, 9999, 'BROTO JARDINEIRA', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2135, 9999, 'BROTO JUNIOR', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2136, 9999, 'BROTO LIGHT', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2137, 9999, 'BROTO LOMBO', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2138, 9999, 'BROTO LOMBO CATUPIRY', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2139, 9999, 'BROTO MARGUERITA', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2140, 9999, 'BROTO MILHO VERDE', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2141, 9999, 'BROTO MUSSARELA', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2142, 9999, 'BROTO NAPOLITANA', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2143, 9999, 'BROTO NOVA CALABRESA', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2144, 9999, 'BROTO PALMITO', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2145, 9999, 'BROTO PEPPERONI', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2146, 9999, 'BROTO PERUANA I', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2147, 9999, 'BROTO PERUANA II', 99, '28.00', 'no_image.png', '0', '28.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2148, 9999, 'BROTO POMODORO', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2149, 9999, 'BROTO PORTUGUESA', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2150, 9999, 'BROTO PORTUGUESINHA', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2151, 9999, 'BROTO QUATRO FORMAGGI', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2152, 9999, 'BROTO ROMANA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2153, 9999, 'BROTO ROMANESCA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2154, 9999, 'BROTO RUCULA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2155, 9999, 'BROTO SICILIANA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2156, 9999, 'BROTO TOSCANA', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2157, 9999, 'BROTO VEGETARIANA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2158, 9999, 'BROTO VENEZA', 99, '27.00', 'no_image.png', '0', '27.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2159, 9999, 'BROTO BANANA', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2160, 9999, 'BROTO BRIGADEIRO', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2161, 9999, 'BROTO PRESTIGIO', 99, '25.00', 'no_image.png', '0', '25.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2162, 9999, 'BROTO ROMEU E JULIETA', 99, '20.00', 'no_image.png', '0', '20.00', 0, 1, '', '0', '', '0.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2163, 0, '1/2 BROTO ATUM 1/2 BROTO MUSSARELA', 100, '0.00', 'no_image.png', '0', '25.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2164, 0, '1/2 ALHO 1/2 SICILIANA', 100, '0.00', 'no_image.png', '0', '35.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2165, 0, '1/2 PORTUGUESA 1/2 CALABRESA', 100, '0.00', 'no_image.png', '0', '33.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2166, 0, '1/2 AMERICANA 1/2 FRANGO CATUPIRY', 100, '0.00', 'no_image.png', '0', '36.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2167, 0, '1/2 MARGUERITA 1/2 GREGA', 100, '0.00', 'no_image.png', '0', '35.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2168, 0, '1/2 CINCO FORMAGGI 1/2 PERUANA I', 100, '0.00', 'no_image.png', '0', '36.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2169, 0, '1/2 CALABRESA 1/2 PORTUGUESA', 100, '0.00', 'no_image.png', '0', '33.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2170, 0, '1/2 DI PIZZAIOLO II 1/2 MARGUERITA', 100, '0.00', 'no_image.png', '0', '37.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2171, 623, 'COCA-COLA 2LT', 6, '0.00', 'no_image.png', '0', '10.00', 0, -1, '', '1', '', '5.00', 0, 0, '19023000', '5102', '0', '0', '0', '0', '0', '0', '0'),
(2173, 0, '1/2 CINCO FORMAGGI 1/2 PEPPERONI', 100, '0.00', 'no_image.png', '0', '36.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2174, 0, '1/2 PALMITO 1/2 MARGUERITA', 100, '0.00', 'no_image.png', '0', '33.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2175, 0, '1/2 CALABRESA 1/2 MARGUERITA', 100, '0.00', 'no_image.png', '0', '32.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2176, 0, '1/2 MUSSARELA 1/2 CALABRESA', 100, '0.00', 'no_image.png', '0', '29.00', 0, 10, '', '', '', '5.00', 0, 0, '19023000', '5102', '', '', '', '', '', '', ''),
(2177, 624, 'GUARANA ANTARCTICA 2LT', 6, '0.00', 'no_image.png', '0', '10.00', 0, -2, '', '1', '', '5.00', 1, 0, '19023000', '5102', '0', '0', '0', '0', '0', '0', '0'),
(2178, 57, 'FOGAZZA MUSSA E CAL', 3, '0.00', 'no_image.png', '0', '10.00', 0, -1, '', '1', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2179, 58, 'FOGAZZA 2 RECHEIOS', 3, '0.00', 'no_image.png', '0', '13.00', 0, -2, '', '1', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2180, 977, 'PIZZA MUSSA/CAL PROMOCAO RETIRA', 1, '0.00', 'no_image.png', '0', '25.00', 0, 0, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2181, 977, 'PIZZA MUSSA/CAL PROMOCAO RETIRA', 98, '0.00', 'no_image.png', '0', '25.00', 0, 0, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2182, 977, 'PIZZA MUSSA/CAL PROMOCAO RETIRA', 99, '0.00', 'no_image.png', '0', '25.00', 0, 0, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2183, 978, 'CALABRESA II', 1, '0.00', 'no_image.png', '0', '27.00', 0, 0, '', '0', '', '5.00', 0, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2184, 978, 'CALABRESA ESPECIAL', 98, '0.00', 'no_image.png', '0', '28.00', 0, 0, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2185, 978, 'CALABRESA ESPECIAL', 99, '0.00', 'no_image.png', '0', '28.00', 0, 0, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2186, 979, 'MUSSARELA II', 1, '0.00', 'no_image.png', '0', '28.00', 0, 0, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2187, 979, 'MUSSARELA II', 98, '0.00', 'no_image.png', '0', '28.00', 0, 0, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2188, 979, 'MUSSARELA II', 99, '0.00', 'no_image.png', '0', '28.00', 0, 0, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2189, 980, 'MUSSARELA//CALABRESA PROMOÇAO', 1, '0.00', 'no_image.png', '0', '26.00', 0, 1, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2190, 980, 'MUSSARELA//CALABRESA PROMOÇAO', 98, '0.00', 'no_image.png', '0', '26.00', 0, 1, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2191, 980, 'MUSSARELA//CALABRESA PROMOÇAO', 99, '0.00', 'no_image.png', '0', '26.00', 0, 1, '', '0', '', '5.00', 1, 0, '19023000', '5102', '0', '500', '0', '0', '49', '0', '49'),
(2192, 0, '1/2 MUSSARELA//CALABRESA PROMOÇAO 1/2 MUSSARELA//CALABRESA PROMOÇ', 100, '0.00', 'no_image.png', '0', '26.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2193, 0, '1/2 MUSSARELA//CALABRESA PROMOÇAO 1/2 MUSSARELA//CALABRESA PROMOÇ', 100, '0.00', 'no_image.png', '0', '26.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2194, 0, '1/2 MUSSARELA//CALABRESA PROMOÇAO 1/2 MUSSARELA//CALABRESA PROMOÇ', 100, '0.00', 'no_image.png', '0', '26.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2195, 0, '1/2 MUSSARELA//CALABRESA PROMOÇAO 1/2 MUSSARELA//CALABRESA PROMOÇ', 100, '0.00', 'no_image.png', '0', '26.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2196, 0, '1/2-ATUM/FRANGO CATUPIRY', 100, '0.00', 'no_image.png', '0', '33.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2197, 0, '1/2 BAIANA 1/2 PEPPERONI', 100, '0.00', 'no_image.png', '0', '36.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2198, 0, '1/2 TOSCANA 1/2 FRANGO CATUPIRY', 100, '0.00', 'no_image.png', '0', '33.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2199, 0, '1/2 BACON 1/2 TOSCANA', 100, '0.00', 'no_image.png', '0', '33.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2200, 0, '1/2 MARGUERITA 1/2 CALABRESA', 100, '0.00', 'no_image.png', '0', '32.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2201, 0, '1/2 BROTO CALABRESA 1/2 BROTO CATUPIRY', 100, '0.00', 'no_image.png', '0', '20.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2202, 0, '1/2-BAIANA/FRANGO CATUPIRY', 100, '0.00', 'no_image.png', '0', '33.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2203, 0, '1/2 FRANGO CATUPIRY 1/2 ATUM ESPECIAL', 100, '0.00', 'no_image.png', '0', '34.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2204, 0, '1/2 BAIANA 1/2 PORTUGUESA', 100, '0.00', 'no_image.png', '0', '33.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', ''),
(2205, 0, '1/2 FRANGO CATUPIRY 1/2 TOSCANA', 100, '0.00', 'no_image.png', '0', '33.00', 0, 10, '', '', '', '5.00', 0, 0, '', '5405', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(100) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `avatar` varchar(150) NOT NULL,
  `id_cargo` int(11) DEFAULT NULL,
  `ativo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `avatar`, `id_cargo`, `ativo`) VALUES
(1, 'Junior Nascimento', 'jjunior', 'ejwkh24', 'images/boy.png', 1, 1),
(12, 'Operadora 1 ', 'operadora', '12345678', 'images/girl.png', 2, 0),
(14, 'Celso', 'motoboy', '12345678', 'images/boy.png', 5, 1),
(15, 'Joaquim', '', '', 'images/boy.png', 5, 1),
(16, '-', '', '', '', 5, 1),
(17, 'Gerente ', 'admin', '123456', 'images/boy.png', 2, 1),
(18, 'Operador 2 ', 'caixa2', 'caixa2', 'images/boy.png', 2, 1),
(19, 'Operador ', 'balcao', '123456', 'images/boy.png', 2, 1),
(20, 'FLAVIO ', 'admin', '123456', 'images/boy.png', 5, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `valores_nota`
--

CREATE TABLE `valores_nota` (
  `id` int(11) NOT NULL,
  `troco` varchar(45) NOT NULL,
  `forma_pagamento` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_history`
--

CREATE TABLE `vendas_history` (
  `id` int(11) NOT NULL,
  `code` int(10) NOT NULL,
  `Produto` varchar(65) CHARACTER SET utf8 NOT NULL,
  `quantidade` int(11) NOT NULL,
  `Preço` decimal(25,2) DEFAULT NULL,
  `Total` decimal(35,2) DEFAULT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `id_produto` int(11) NOT NULL DEFAULT '0',
  `num_nota_fiscal` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vendas_history`
--

INSERT INTO `vendas_history` (`id`, `code`, `Produto`, `quantidade`, `Preço`, `Total`, `obs`, `category_id`, `id_produto`, `num_nota_fiscal`) VALUES
(1, 1, 'ESFIHA CARNE', 1, '0.99', '0.99', '', 1, 1, NULL),
(2, 575, 'AMERICANA', 1, '31.00', '31.00', '31.00', 99, 311, NULL),
(3, 580, 'BRASILEIRA', 1, '30.00', '30.00', '', 99, 316, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_motoboys`
--

CREATE TABLE `vendas_motoboys` (
  `id` int(11) NOT NULL,
  `id_motoboy` int(11) NOT NULL,
  `entregas` int(11) NOT NULL,
  `total_taxas` decimal(10,2) DEFAULT NULL,
  `id_abertura` int(11) NOT NULL,
  `horario` varchar(45) DEFAULT NULL,
  `pago` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `vendas_motoboys`
--

INSERT INTO `vendas_motoboys` (`id`, `id_motoboy`, `entregas`, `total_taxas`, `id_abertura`, `horario`, `pago`) VALUES
(1, 14, 1, '2.00', 20, '2017-02-15 21:43', 1),
(2, 14, 1, '4.00', 20, '2017-02-15 21:43', 1),
(3, 15, 1, '2.00', 20, '2017-02-15 21:44', 1),
(4, 15, 1, '2.00', 20, '2017-02-15 21:44', 1),
(5, 15, 1, '2.00', 20, '2017-02-15 22:45', 1),
(6, 15, 1, '2.00', 21, '2017-02-17 22:13', 1),
(7, 14, 1, '4.00', 21, '2017-02-17 22:20', 1),
(8, 0, 1, '2.00', 23, '2017-03-06 22:28', 0),
(9, 0, 1, '2.00', 23, '2017-03-07 00:54', 0),
(10, 0, 1, '2.00', 23, '2017-03-07 01:22', 0),
(11, 0, 1, '2.00', 23, '2017-03-07 01:27', 0),
(12, 0, 1, '2.00', 23, '2017-03-07 18:29', 0),
(13, 15, 1, '2.00', 23, '2017-03-07 22:36', 0),
(14, 0, 1, '2.00', 23, '2017-03-07 22:41', 0),
(15, 15, 1, '3.00', 24, '2017-03-17 00:11', 0),
(16, 14, 1, '2.00', 26, '2017-03-20 13:00', 0),
(17, 14, 1, '4.00', 26, '2017-03-20 22:03', 0),
(18, 0, 1, '3.00', 26, '2017-03-22 18:06', 0),
(19, 0, 1, '2.00', 26, '2017-03-22 18:06', 0),
(20, 15, 1, '2.00', 26, '2017-03-25 17:23', 0),
(21, 14, 1, '4.00', 27, '2017-04-06 22:14', 0),
(22, 15, 1, '2.00', 31, '2017-04-12 21:38', 0),
(23, 0, 1, '2.00', 31, '2017-04-13 00:11', 0),
(24, 15, 1, '2.00', 31, '2017-04-13 00:44', 0),
(25, 15, 1, '2.00', 31, '2017-04-13 00:45', 0),
(26, 14, 1, '4.00', 31, '2017-04-13 00:50', 0),
(27, 14, 1, '4.00', 31, '2017-04-13 01:27', 0),
(28, 14, 1, '4.00', 31, '2017-04-17 15:11', 0),
(29, 0, 1, '2.00', 31, '2017-04-18 00:52', 0),
(30, 15, 1, '2.00', 31, '2017-04-18 00:53', 0),
(31, 14, 1, '2.00', 31, '2017-04-24 15:44', 0),
(32, 14, 1, '2.00', 32, '2017-04-24 22:08', 0),
(33, 16, 1, '3.00', 32, '2017-04-25 07:25', 0),
(34, 16, 1, '2.00', 32, '2017-04-25 07:27', 0),
(35, 16, 1, '3.00', 32, '2017-04-25 07:30', 0),
(36, 14, 1, '3.00', 64, '2017-06-24 10:24', 0),
(37, 16, 1, '0.00', 71, '2017-06-24 12:34', 0),
(38, 16, 1, '0.00', 71, '2017-06-24 12:37', 0),
(39, 16, 1, '0.00', 71, '2017-06-24 12:50', 0),
(40, 16, 1, '0.00', 71, '2017-06-24 12:53', 0),
(41, 16, 1, '0.00', 71, '2017-06-24 12:55', 0),
(42, 16, 1, '0.00', 71, '2017-06-24 12:57', 0),
(43, 16, 1, '0.00', 71, '2017-06-24 13:24', 0),
(44, 16, 1, '0.00', 71, '2017-06-24 13:26', 0),
(45, 16, 1, '0.00', 71, '2017-06-24 13:36', 0),
(46, 16, 1, '0.00', 71, '2017-06-24 14:11', 0),
(47, 16, 1, '0.00', 71, '2017-06-24 14:23', 0),
(48, 16, 1, '0.00', 71, '2017-06-24 14:33', 0),
(49, 16, 1, '0.00', 71, '2017-06-24 15:20', 0),
(50, 16, 1, '0.00', 71, '2017-06-24 15:42', 0),
(51, 16, 1, '0.00', 71, '2017-06-24 16:08', 0),
(52, 16, 1, '0.00', 71, '2017-06-24 16:34', 0),
(53, 16, 1, '3.00', 74, '2017-06-25 10:18', 0),
(54, 16, 1, '0.00', 75, '2017-06-25 14:32', 0),
(55, 16, 1, '0.00', 75, '2017-06-25 14:36', 0),
(56, 16, 1, '0.00', 75, '2017-06-25 15:10', 0),
(57, 16, 1, '2.00', 85, '2017-09-13 20:47', 0),
(58, 16, 1, '2.00', 85, '2017-09-13 22:43', 0),
(59, 14, 1, '2.00', 85, '2017-09-13 22:50', 0),
(60, 16, 1, '2.00', 85, '2017-09-13 22:51', 0),
(61, 16, 1, '3.00', 86, '2017-09-14 20:18', 0),
(62, 16, 1, '3.00', 89, '2017-09-14 21:29', 0),
(63, 16, 1, '3.00', 91, '2017-09-15 21:16', 0),
(64, 16, 1, '3.00', 91, '2017-09-15 21:57', 0),
(65, 16, 1, '0.00', 91, '2017-09-15 22:03', 0),
(66, 16, 1, '3.00', 92, '2017-09-16 20:10', 0),
(67, 16, 1, '0.00', 92, '2017-09-17 18:15', 0),
(68, 16, 1, '3.00', 92, '2017-09-17 19:16', 0),
(69, 16, 1, '3.00', 92, '2017-09-17 19:40', 0),
(70, 16, 1, '3.00', 92, '2017-09-17 21:23', 0),
(71, 16, 1, '3.00', 93, '2017-09-18 20:51', 0),
(72, 16, 1, '3.00', 93, '2017-09-18 21:15', 0),
(73, 16, 1, '0.00', 94, '2017-09-19 20:29', 0),
(74, 16, 1, '3.00', 94, '2017-09-19 20:31', 0),
(75, 16, 1, '0.00', 94, '2017-09-19 20:41', 0),
(76, 16, 1, '0.00', 94, '2017-09-19 20:44', 0),
(77, 16, 1, '0.00', 94, '2017-09-19 22:10', 0),
(78, 16, 1, '0.00', 94, '2017-09-19 22:23', 0),
(79, 16, 1, '0.00', 95, '2017-09-20 20:04', 0),
(80, 16, 1, '3.00', 95, '2017-09-20 20:42', 0),
(81, 16, 1, '0.00', 95, '2017-09-20 21:29', 0),
(82, 16, 1, '0.00', 95, '2017-09-20 22:48', 0),
(83, 16, 1, '3.00', 95, '2017-09-20 22:54', 0),
(84, 16, 1, '0.00', 96, '2017-09-21 19:05', 0),
(85, 16, 1, '3.00', 96, '2017-09-21 19:08', 0),
(86, 16, 1, '3.00', 96, '2017-09-21 19:16', 0),
(87, 16, 1, '0.00', 96, '2017-09-21 19:25', 0),
(88, 16, 1, '0.00', 96, '2017-09-21 19:44', 0),
(89, 16, 1, '0.00', 96, '2017-09-21 20:29', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_suspensas`
--

CREATE TABLE `vendas_suspensas` (
  `id` int(11) NOT NULL,
  `id_suspensao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_suspensao` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda_balcao_hist`
--

CREATE TABLE `venda_balcao_hist` (
  `id` int(11) NOT NULL,
  `num_venda` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `data_venda` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `forma_pagamento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_dinheiro` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `venda_balcao_hist`
--

INSERT INTO `venda_balcao_hist` (`id`, `num_venda`, `id_produto`, `quantidade`, `obs`, `data_venda`, `forma_pagamento`, `total_dinheiro`) VALUES
(1, 1, 1, 1, '', '2017-01-16 15:22', 'Cartão de Débito', NULL),
(2, 1, 15, 1, '', '2017-01-16 15:22', 'Cartão de Débito', NULL),
(3, 1, 5, 1, '', '2017-01-16 19:28', 'Dinheiro', NULL),
(4, 1, 17, 1, '', '2017-01-16 19:28', 'Dinheiro', NULL),
(5, 2, 3, 1, '', '2017-01-16 19:59', 'Dinheiro', NULL),
(6, 2, 4, 1, '', '2017-01-16 19:59', 'Dinheiro', NULL),
(7, 3, 5, 2, '', '2017-01-16 22:45', '', NULL),
(8, 4, 12, 1, '', '2017-01-16 22:47', '', NULL),
(9, 5, 10, 1, '', '2017-01-16 22:48', 'Cartão de Crédito', NULL),
(10, 6, 1, 2, '', '2017-01-21 17:26', 'Dinheiro   Crédito', NULL),
(11, 7, 5, 4, '', '2017-01-21 17:31', 'Dinheiro / Débito', NULL),
(12, 8, 5, 3, '', '2017-01-22 18:36', 'Dinheiro', NULL),
(13, 9, 8, 7, '', '2017-01-22 18:47', 'Dinheiro', NULL),
(14, 10, 1, 10, '', '2017-01-23 18:41', 'Dinheiro', NULL),
(15, 11, 2, 1, '', '2017-01-23 19:21', 'Dinheiro', NULL),
(16, 12, 5, 1, '', '2017-01-23 19:22', 'Dinheiro', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `caixa01`
--
ALTER TABLE `caixa01`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `caixa02`
--
ALTER TABLE `caixa02`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `cpf_nota`
--
ALTER TABLE `cpf_nota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forma_pagamento`
--
ALTER TABLE `forma_pagamento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movimentacao_caixa01`
--
ALTER TABLE `movimentacao_caixa01`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nome_nota`
--
ALTER TABLE `nome_nota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pde_fato_vendas`
--
ALTER TABLE `pde_fato_vendas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pde_fato_vendas_produtos`
--
ALTER TABLE `pde_fato_vendas_produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pde_movimentacao`
--
ALTER TABLE `pde_movimentacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa1`
--
ALTER TABLE `pedido_aux_mesa1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa2`
--
ALTER TABLE `pedido_aux_mesa2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa3`
--
ALTER TABLE `pedido_aux_mesa3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa4`
--
ALTER TABLE `pedido_aux_mesa4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa5`
--
ALTER TABLE `pedido_aux_mesa5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa6`
--
ALTER TABLE `pedido_aux_mesa6`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa7`
--
ALTER TABLE `pedido_aux_mesa7`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa8`
--
ALTER TABLE `pedido_aux_mesa8`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa9`
--
ALTER TABLE `pedido_aux_mesa9`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa10`
--
ALTER TABLE `pedido_aux_mesa10`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa11`
--
ALTER TABLE `pedido_aux_mesa11`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa12`
--
ALTER TABLE `pedido_aux_mesa12`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa13`
--
ALTER TABLE `pedido_aux_mesa13`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa14`
--
ALTER TABLE `pedido_aux_mesa14`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa15`
--
ALTER TABLE `pedido_aux_mesa15`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa16`
--
ALTER TABLE `pedido_aux_mesa16`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa17`
--
ALTER TABLE `pedido_aux_mesa17`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa18`
--
ALTER TABLE `pedido_aux_mesa18`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa19`
--
ALTER TABLE `pedido_aux_mesa19`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa20`
--
ALTER TABLE `pedido_aux_mesa20`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_balcao`
--
ALTER TABLE `pedido_balcao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_balcao2`
--
ALTER TABLE `pedido_balcao2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_delivery`
--
ALTER TABLE `pedido_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa1`
--
ALTER TABLE `pedido_mesa1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa2`
--
ALTER TABLE `pedido_mesa2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa3`
--
ALTER TABLE `pedido_mesa3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa4`
--
ALTER TABLE `pedido_mesa4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa5`
--
ALTER TABLE `pedido_mesa5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa6`
--
ALTER TABLE `pedido_mesa6`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa7`
--
ALTER TABLE `pedido_mesa7`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa8`
--
ALTER TABLE `pedido_mesa8`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa9`
--
ALTER TABLE `pedido_mesa9`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa10`
--
ALTER TABLE `pedido_mesa10`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa11`
--
ALTER TABLE `pedido_mesa11`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa12`
--
ALTER TABLE `pedido_mesa12`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa13`
--
ALTER TABLE `pedido_mesa13`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa14`
--
ALTER TABLE `pedido_mesa14`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa15`
--
ALTER TABLE `pedido_mesa15`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa16`
--
ALTER TABLE `pedido_mesa16`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa17`
--
ALTER TABLE `pedido_mesa17`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa18`
--
ALTER TABLE `pedido_mesa18`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa19`
--
ALTER TABLE `pedido_mesa19`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa20`
--
ALTER TABLE `pedido_mesa20`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos_suspensos`
--
ALTER TABLE `produtos_suspensos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retorno_sat`
--
ALTER TABLE `retorno_sat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sabores_pizza`
--
ALTER TABLE `sabores_pizza`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `senhas`
--
ALTER TABLE `senhas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `senhas_painel`
--
ALTER TABLE `senhas_painel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_config`
--
ALTER TABLE `s_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabela_auxiliar_venda`
--
ALTER TABLE `tabela_auxiliar_venda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_products`
--
ALTER TABLE `tec_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `valores_nota`
--
ALTER TABLE `valores_nota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendas_history`
--
ALTER TABLE `vendas_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendas_motoboys`
--
ALTER TABLE `vendas_motoboys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendas_suspensas`
--
ALTER TABLE `vendas_suspensas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venda_balcao_hist`
--
ALTER TABLE `venda_balcao_hist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `caixa01`
--
ALTER TABLE `caixa01`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `caixa02`
--
ALTER TABLE `caixa02`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cpf_nota`
--
ALTER TABLE `cpf_nota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forma_pagamento`
--
ALTER TABLE `forma_pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `movimentacao_caixa01`
--
ALTER TABLE `movimentacao_caixa01`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nome_nota`
--
ALTER TABLE `nome_nota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pde_fato_vendas`
--
ALTER TABLE `pde_fato_vendas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `pde_fato_vendas_produtos`
--
ALTER TABLE `pde_fato_vendas_produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `pde_movimentacao`
--
ALTER TABLE `pde_movimentacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa1`
--
ALTER TABLE `pedido_aux_mesa1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa2`
--
ALTER TABLE `pedido_aux_mesa2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa3`
--
ALTER TABLE `pedido_aux_mesa3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa4`
--
ALTER TABLE `pedido_aux_mesa4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa5`
--
ALTER TABLE `pedido_aux_mesa5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa6`
--
ALTER TABLE `pedido_aux_mesa6`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa7`
--
ALTER TABLE `pedido_aux_mesa7`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa8`
--
ALTER TABLE `pedido_aux_mesa8`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa9`
--
ALTER TABLE `pedido_aux_mesa9`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa10`
--
ALTER TABLE `pedido_aux_mesa10`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa11`
--
ALTER TABLE `pedido_aux_mesa11`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa12`
--
ALTER TABLE `pedido_aux_mesa12`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa13`
--
ALTER TABLE `pedido_aux_mesa13`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa14`
--
ALTER TABLE `pedido_aux_mesa14`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa15`
--
ALTER TABLE `pedido_aux_mesa15`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa16`
--
ALTER TABLE `pedido_aux_mesa16`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa17`
--
ALTER TABLE `pedido_aux_mesa17`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa18`
--
ALTER TABLE `pedido_aux_mesa18`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa19`
--
ALTER TABLE `pedido_aux_mesa19`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa20`
--
ALTER TABLE `pedido_aux_mesa20`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_balcao`
--
ALTER TABLE `pedido_balcao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_balcao2`
--
ALTER TABLE `pedido_balcao2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_delivery`
--
ALTER TABLE `pedido_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pedido_mesa1`
--
ALTER TABLE `pedido_mesa1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa2`
--
ALTER TABLE `pedido_mesa2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa3`
--
ALTER TABLE `pedido_mesa3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa4`
--
ALTER TABLE `pedido_mesa4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa5`
--
ALTER TABLE `pedido_mesa5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa6`
--
ALTER TABLE `pedido_mesa6`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa7`
--
ALTER TABLE `pedido_mesa7`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa8`
--
ALTER TABLE `pedido_mesa8`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa9`
--
ALTER TABLE `pedido_mesa9`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa10`
--
ALTER TABLE `pedido_mesa10`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa11`
--
ALTER TABLE `pedido_mesa11`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa12`
--
ALTER TABLE `pedido_mesa12`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa13`
--
ALTER TABLE `pedido_mesa13`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa14`
--
ALTER TABLE `pedido_mesa14`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa15`
--
ALTER TABLE `pedido_mesa15`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa16`
--
ALTER TABLE `pedido_mesa16`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa17`
--
ALTER TABLE `pedido_mesa17`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa18`
--
ALTER TABLE `pedido_mesa18`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa19`
--
ALTER TABLE `pedido_mesa19`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa20`
--
ALTER TABLE `pedido_mesa20`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produtos_suspensos`
--
ALTER TABLE `produtos_suspensos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retorno_sat`
--
ALTER TABLE `retorno_sat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sabores_pizza`
--
ALTER TABLE `sabores_pizza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `senhas`
--
ALTER TABLE `senhas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `senhas_painel`
--
ALTER TABLE `senhas_painel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_config`
--
ALTER TABLE `s_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tabela_auxiliar_venda`
--
ALTER TABLE `tabela_auxiliar_venda`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tec_products`
--
ALTER TABLE `tec_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2206;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `valores_nota`
--
ALTER TABLE `valores_nota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendas_history`
--
ALTER TABLE `vendas_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vendas_motoboys`
--
ALTER TABLE `vendas_motoboys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `vendas_suspensas`
--
ALTER TABLE `vendas_suspensas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `venda_balcao_hist`
--
ALTER TABLE `venda_balcao_hist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
